/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package raz.games.tic_tac_toe;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author razvan
 */
public class GameXOAITest {

    public GameXOAITest() {
    }
    public static final byte mark_neutral = 0;
    public static final byte mark_x = 1;
    public static final byte mark_o = 2;

    /**
     * Test of initStateAI method, of class GameXOAI.
     */
    @Test
    public void testInitStateAI() {
        System.out.println(" JUnit Test initStateAI");
        
        byte[][] matrixState = new byte[][]{
            {0, 1, 0},
            {0, 0, 0},
            {0, 0, 0}
        };
        byte mark_AI = 2;
        GameXOAI instanceAI = new GameXOAI();
        instanceAI.initStateAI(matrixState, mark_AI);
        instanceAI.showAllPossibleSolutionList();
        // TODO review the generated test code and remove the default call to fail.
        
    }
}