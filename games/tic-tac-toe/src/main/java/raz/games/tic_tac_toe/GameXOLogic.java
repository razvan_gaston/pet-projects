/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package raz.games.tic_tac_toe;

/**
 *
 * @author razvan
 * this is the Game Class that contains the mathematical game logic/mathematical model
 */
public class GameXOLogic {

    public static final byte mark_neutral = 0;
    public static final byte mark_x = 1;
    public static final byte mark_o = 2;
    public static final char player_x = 'x';
    public static final char player_o = 'o';

    //The Matrix of the Game
    private byte[][] gameMatrix = new byte[3][3];
    private char player = player_x;
    /*
     * care vector din matrice e castigator
     * 1,2,3 => linia 1,2 sau 3 ( 1 + indexul_Liniei)
     * 4,5,6 => coloana 1,2 sau 3 (4 + indexul_Coloanei)
     * 7 => diagonala principala
     * 8 => diagonala secundara
     */
    private int winnerVector = 0;

    public int getWinnerVector() {
        return winnerVector;
    }

    public char getPlayer() {
        return player;
    }

    public byte[][] getGameMatrix() {
        return gameMatrix;
    }

    public void changePlayer() {
        if (this.player == player_x) {
            this.player = player_o;
        } else if (this.player == player_o) {
            this.player = player_x;
        }
    }

    /**
     * This Method initializez a Matrix with a given value.
     * It iterates through the whole matrix and sets it's cells to 
     * the value received as a parameter
     * @param matrix = the matrix that is to be initialized
     * @param  value = the value that will be set to all of the matrix cells
     * @return the initialized matrix 
     */
    public static byte[][] initMatrix(byte[][] matrix, byte value) {
        if (matrix == null) {
            throw new IllegalArgumentException("The matrix can not be null");
        }
        int numberOfLines = matrix.length;
        for(int i=0;i<numberOfLines;i++){
            byte[] oneLine = matrix[i];
            int len = oneLine.length;
            for(int j=0; j<len;j++){
                oneLine[j]=value;
            }
        }
        return matrix;
    }

    /*
     *Metoda care initializeaza matricea (si winnerVector) cu 0, adica neutral 
     * 
     */
    private void initMatrix() {
        gameMatrix = initMatrix(gameMatrix,mark_neutral);
        /*
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                gameMatrix[i][j] = mark_neutral;
            }
        }
        */
    }


    /*
     *Constructorul care apeleaza initMatrix
     * 
     */
    public GameXOLogic() {
        this.initMatrix();
    }

    /**
     * Metoda care Marcheaza o anumita celula a matricei cu o valoarea mark_x sau mark_o
     * @param [row,column]
     */
    public void markCell(int row, int column, byte value) {
        if (row < 3 && column < 3 && value < 3) {
            gameMatrix[row][column] = value;
        }
    }

    /**
     *Metoda care returneaza valoarea unei celule
     * @param [row,column]
     * @return cell value
     */
    public byte getCellValue(int row, int column) {
        if (row < 3 && column < 3) {
            return gameMatrix[row][column];
        } else {
            return -1;
        }
    }

    /**
     *Metoda care primeste ca parametru un vector si determina daca vectorul este castigator
     * adica daca toate elem. vectorului sunt fie 'x' fie 'o'
     * @param byte[] vector
     * @return is the vector winner ? true/flase
     */
    private static boolean isVectorWiner(byte[] vector) {
        boolean isWon = true;
        byte winValue = vector[0];
        if (winValue == mark_neutral) {
            /*daca prima celula e neutra, atunci linia asta Nu poate fi castigatoare */
            isWon = false;
            return isWon;
        }
        for (int j = 1; j <= 2; j++) {
            if (vector[j] == mark_neutral) {
                /*daca vreuna din celule parcurse e neutra, atunci linia NU e castigatore*/
                isWon = false;
                return isWon;
            }
            if (vector[j] != winValue) {
                /*daca vreuna din celule parcurse difera de winValue atunci linia nu e castigatoare*/
                isWon = false;
                return isWon;
            }
        }
        /*daca toate conditiile esueasza atunci isWon ramane true!*/

        //System.out.println("Joc castigat de jucatorul:" + this.player);
        return isWon;
    }

    /**
     * Metoda care parcurge Matricea pe -linii, -coloane, -Diag. Princip., -Diag. Sec. Si verifica
     * daca fiecare line/coloana/diagonala este castigatoare
     * @return daca return este mai mare ca zero, atunci jocul a fost castigat. daca return este 0 atunci jocul nu a fost castigat
     */
    public static int isGameWon(byte[][] matrix) {
        int winnerVector = 0;
        boolean isWon = false;
        //Verificare pe linie
        int numberOfLines = matrix.length;
        for(int i=0;i<numberOfLines;i++){
            byte[] oneLine = matrix[i];
            isWon = isVectorWiner(oneLine);
            if (isWon) {
                winnerVector = 1 + i;
                System.out.println(" Castigat pe linia:" + winnerVector);
                return winnerVector;
            }
        }
        

        //Verificare pe coloana
        for (int i = 0; i < 3; i++) {
            byte[] o_coloana = new byte[3];
            for (int j = 0; j < 3; j++) {
                o_coloana[j] = matrix[j][i];
            }
            isWon = isVectorWiner(o_coloana);
            if (isWon) {
                winnerVector = 4 + i;
                System.out.println("Castigat pe coloana " + winnerVector);
                return winnerVector;
            }
        }
        //Verificarea pe diagolana principala
        byte[] diag_principala = new byte[3];
        for (int i = 0; i < 3; i++) {
            diag_principala[i] = matrix[i][i];
        }
        isWon = isVectorWiner(diag_principala);
        if (isWon) {
            winnerVector = 7;
            System.out.println("Castigat pe Diag Principala-" + winnerVector);
            return winnerVector;
        }

        //Verificare pe diagonala secundara
        byte[] diag_secundara = new byte[3];
        for (int i = 0; i < 3; i++) {
            diag_secundara[i] = matrix[i][2 - i];
        }
        isWon = isVectorWiner(diag_secundara);
        if (isWon) {
            winnerVector = 8;
            System.out.println("Castigat pe Diag Secundara-" + winnerVector);
            return winnerVector;
        }

        return winnerVector;
    }
    /**
     * Metoda care determina pentru instanta aceasta de joc, daca jocul a fost castigat sau nu
     * 
     * @return true daca jocul a fost castigat, flase daca nu
     */
    public boolean isGameWon(){
        int result = isGameWon(gameMatrix);
        if(result>0){
            this.winnerVector = result;
            return true;
        } else {
            return false;
        }
    }
}
