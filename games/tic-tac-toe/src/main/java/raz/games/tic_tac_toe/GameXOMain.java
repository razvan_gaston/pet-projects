/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package raz.games.tic_tac_toe;


/**
 *
 * @author razvan
 */
public class GameXOMain {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                GameXOGUI.createAndShowGUI();
            }
        });

    }
}
