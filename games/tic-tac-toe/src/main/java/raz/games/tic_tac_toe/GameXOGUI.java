/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package raz.games.tic_tac_toe;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author razvan
 */
public class GameXOGUI {

    //Create and set up the window.
    static JFrame frame = new JFrame("Game x si o");
    //JPANEL
    static JPanel panel = new JPanel();
    // matricea de butoane
    static JButton[] buttonMatrix = new JButton[9];
    //
    static GameXOLogic game = new GameXOLogic();
    public static boolean PLAY_VS_AI = true;

    public static void createAndShowGUI() {

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(20, 20, 200, 200);
        panel.setLayout(new GridLayout(3, 3));
        panel.setBackground(Color.GREEN);

        //Generate 9 JButtons
        //Assign to each button an action-command string from 0 to 8
        //Add ActionListener to each button
        //Adds button to panel
        for (int i = 0; i < buttonMatrix.length; i++) {
            JButton oneButton = new JButton(" ");
            oneButton.setActionCommand(String.valueOf(i));
            oneButton.addActionListener(new actListener());
            buttonMatrix[i] = oneButton;
            System.out.println(GameXOGUI.class + " butonul " + i + " are acction Comandul: " + buttonMatrix[i].getActionCommand());
            panel.add(buttonMatrix[i]);
        }

        //Display the window.
        frame.getContentPane().add(panel);
        //frame.pack();
        frame.setVisible(true);
    }

    /**
     * gets the ActionCommand of this button
     * @param int button index from 0 to 8
     * @return
     */
    public static String getActionCommand(int button) {
        if (button >= 0 && button < 9) {
            return buttonMatrix[button].getActionCommand();
        } else {
            return "";
        }
    }

    /**
     * Sets the Visible Text on the button. I can be "X" or "O"
     * @param button - the index [0,8]
     * @param text - can be "X" or "O"
     * @param text
     */
    public static void setButtonText(int button, String text) {
        if (button >= 0 && button < 9) {
            buttonMatrix[button].setText(text);
        }
    }

    /**
     * sets a winner button background color to color red.
     * @param button - the index of the button [0..8]
     */
    public static void setWinButton(int button) {
        if (button >= 0 && button < 9) {
            buttonMatrix[button].setBackground(Color.RED);
        }
    }
}

/*-----------------------------------------------------------------------------
the action Listener class
 */
class actListener implements java.awt.event.ActionListener {

    private byte mark_AI_player = GameXOLogic.mark_neutral;

    public void actionPerformed(java.awt.event.ActionEvent evt) {

        System.out.println(actListener.class + " action command happened:" + evt.getActionCommand());

        if (GameXOGUI.PLAY_VS_AI) {
            //aici imi initializez jucatorul AI
            if (mark_AI_player == GameXOLogic.mark_neutral) {
                if (GameXOGUI.game.getPlayer() == GameXOLogic.player_x) {
                    mark_AI_player = GameXOLogic.mark_o;
                    System.out.println(actListener.class + "am initializat AI player =" + mark_AI_player);
                }
            }
        }

        if (!GameXOGUI.PLAY_VS_AI) {
            int indiceButton = 0;
            int offSet = 0;
            boolean exit = false;
            /*
             * De fiecare data cand un jucator da click, parcurg matricea de butoane
             */
            for (int i = 0; i < 3; i++) {
                if (exit) {
                    break;
                }
                for (int j = 0; j < 3; j++) {
                    if (exit) {
                        break;
                    }
                    if (i == 0) {
                        offSet = 0; //indiceButton = offSet+j

                    } else if (i == 1) {
                        offSet = 3;
                    } else if (i == 2) {
                        offSet = 6;
                    }
                    indiceButton = offSet + j;
                    //System.out.println("************************>>" + indiceButton);
                /*
                     * Aici vad care buton s-a apasat in functie de ActionCommandul butonului Apasat.
                     * Fac o asociere intre (ActionComand-ulu Evenimentului/Butonului care poate fi [0,8]) si 
                     * indicele calculat al butonului care poate fi tot intre [0,8]
                     * cand cele 2 sunt egale, inseamna ca in parcurgere sunt pe butonul care trebui(care a fost apasat)
                     * 
                     */
                    if (evt.getActionCommand().equalsIgnoreCase(GameXOGUI.getActionCommand(indiceButton))) {
                        /*
                         *Am ajuns in matricea de butoane pe butonul care a fost apasat
                         */
                        if (GameXOGUI.game.getPlayer() == GameXOLogic.player_x) {
                            //Daca jucatorul este "x" marchez gameMatrix[i][j] = x
                            GameXOGUI.game.markCell(i, j, GameXOLogic.mark_x);
                            System.out.println("[" + i + "][" + j + "][" + GameXOGUI.game.getPlayer() + "]->" + GameXOLogic.mark_x);
                        } else {
                            //Altfel, Daca jucatorul este "0" marchez gameMatrix[i][j] = 0  
                            GameXOGUI.game.markCell(i, j, GameXOLogic.mark_o);
                            System.out.println("[" + i + "][" + j + "][" + GameXOGUI.game.getPlayer() + "]->" + GameXOLogic.mark_o);
                        }
                        if (GameXOGUI.game.getCellValue(i, j) == GameXOLogic.mark_x) {
                            GameXOGUI.setButtonText(indiceButton, "X");
                        } else {
                            GameXOGUI.setButtonText(indiceButton, "O");
                        }
                        if (GameXOGUI.game.isGameWon()) {
                            System.out.println("Game Castigat");
                            drawWinnerVector();
                        }
                        //GameXOAI ai = new GameXOAI();
                        //ai.initStateAI(GameXOGUI.game.getGameMatrix(), AI_player);
                        GameXOGUI.game.changePlayer();
                        exit = true;
                    }

                }

            }
        }//END IF  NOT PLAY_VS_AI
        else {//PLAY_VS_AI

            System.out.println(actListener.class + " PLAY_VS_AI MODE ON, AI_player=" + mark_AI_player);
            int indiceButton = 0;
            int offSet = 0;
            boolean exit = false;
            /*
             * De fiecare data cand un jucator da click, parcurg matricea de butoane
             */
            for (int i = 0; i < 3; i++) {
                if (exit) {
                    break;
                }
                for (int j = 0; j < 3; j++) {
                    if (exit) {
                        break;
                    }
                    if (i == 0) {
                        offSet = 0; //indiceButton = offSet+j

                    } else if (i == 1) {
                        offSet = 3;
                    } else if (i == 2) {
                        offSet = 6;
                    }
                    indiceButton = offSet + j;
                    //System.out.println("************************>>" + indiceButton);
                /*
                     * Aici vad care buton s-a apasat in functie de ActionCommandul butonului Apasat.
                     * Fac o asociere intre (ActionComand-ulu Evenimentului/Butonului care poate fi [0,8]) si 
                     * indicele calculat al butonului care poate fi tot intre [0,8]
                     * cand cele 2 sunt egale, inseamna ca in parcurgere sunt pe butonul care trebui(care a fost apasat)
                     * 
                     */
                    if (evt.getActionCommand().equalsIgnoreCase(GameXOGUI.getActionCommand(indiceButton))) {
                        /*
                         *Am ajuns in matricea de butoane pe butonul care a fost apasat
                         */
                        /*
                         * Prima data jucatorul este omul/playerul
                         */
                        if (GameXOGUI.game.getPlayer() == GameXOLogic.player_x) {
                            //1. Daca jucatorul este "x" marchez gameMatrix[i][j] = x
                            GameXOGUI.game.markCell(i, j, GameXOLogic.mark_x);
                            System.out.println("[" + i + "][" + j + "][" + GameXOGUI.game.getPlayer() + "]->" + GameXOLogic.mark_x);
                            //2.Setez butonul pe "X"
                            if (GameXOGUI.game.getCellValue(i, j) == GameXOLogic.mark_x) {
                                GameXOGUI.setButtonText(indiceButton, "X");
                            }
                            //3.Verific daca jocul este castigat
                            if (GameXOGUI.game.isGameWon()) {
                                System.out.println("Game Castigat");
                                drawWinnerVector();
                            }
                            //4. Schimb jucatorul
                            GameXOGUI.game.changePlayer();
                            exit = true;


                        }
                        /*
                         * Jucatorul este computerul.
                         * Deci AI-ul trebuie sa genereze o solutie si sa marcheze o celula in game Mattrix
                         */
                        if (GameXOGUI.game.getPlayer() == GameXOLogic.player_o) {
                            indiceButton = 0;
                            offSet = 0;


                            //1. generz solutia 
                            GameXOAI ai = new GameXOAI();
                            ai.initStateAI(GameXOGUI.game.getGameMatrix(), mark_AI_player);
                            ai.showAllPossibleSolutionList();
                            int[] indexesOfAIMove = ai.getSolutionIndexes();
                            ai = null;
                            //Recalculez indicele butonului apasat de Computer
                            if (indexesOfAIMove[0] == 0) {
                                offSet = 0; //indiceButton = offSet+j

                            } else if (indexesOfAIMove[0] == 1) {
                                offSet = 3;
                            } else if (indexesOfAIMove[0] == 2) {
                                offSet = 6;
                            }
                            indiceButton = offSet + indexesOfAIMove[1];

                            GameXOGUI.game.markCell(indexesOfAIMove[0], indexesOfAIMove[1], mark_AI_player);
                            System.out.println("[" + indexesOfAIMove[0] + "][" + indexesOfAIMove[1] + "][" + GameXOGUI.game.getPlayer() + "]->" + mark_AI_player);
                            //2. marchez butonul pe AI_player
                            if (GameXOGUI.game.getCellValue(indexesOfAIMove[0], indexesOfAIMove[1]) == mark_AI_player) {
                                GameXOGUI.setButtonText(indiceButton, "O");
                            }
                            //3.Verific daca jocul este castigat
                            if (GameXOGUI.game.isGameWon()) {
                                System.out.println("Game Castigat");
                                drawWinnerVector();
                            }
                            //4. Schimb jucatorul
                            GameXOGUI.game.changePlayer();
                            exit = true;



                        }


                    }

                }

            }

        }

    }

    private void drawWinnerVector() {
        if (GameXOGUI.game.getWinnerVector() == 1) {
            //linia1[0,2]
            for (int indice = 0; indice < 3; indice++) {
                GameXOGUI.setWinButton(indice);
            }
        } else if (GameXOGUI.game.getWinnerVector() == 2) {
            //linia2[3,5]
            for (int indice = 3; indice <= 5; indice++) {
                GameXOGUI.setWinButton(indice);
            }
        } else if (GameXOGUI.game.getWinnerVector() == 3) {
            //linia3[6,8]
            for (int indice = 6; indice <= 8; indice++) {
                GameXOGUI.setWinButton(indice);
            }
        } else if (GameXOGUI.game.getWinnerVector() == 4) {
            //coloana1[0,3,6]
            GameXOGUI.setWinButton(0);
            GameXOGUI.setWinButton(3);
            GameXOGUI.setWinButton(6);
        } else if (GameXOGUI.game.getWinnerVector() == 5) {
            //coloana2[1,4,7]
            GameXOGUI.setWinButton(1);
            GameXOGUI.setWinButton(4);
            GameXOGUI.setWinButton(7);
        } else if (GameXOGUI.game.getWinnerVector() == 6) {
            //coloana3[0,3,6]
            GameXOGUI.setWinButton(2);
            GameXOGUI.setWinButton(5);
            GameXOGUI.setWinButton(8);
        } else if (GameXOGUI.game.getWinnerVector() == 7) {
            //DIAG Principala
            GameXOGUI.setWinButton(0);
            GameXOGUI.setWinButton(4);
            GameXOGUI.setWinButton(8);
        } else if (GameXOGUI.game.getWinnerVector() == 8) {
            //DIAG Secundara
            GameXOGUI.setWinButton(2);
            GameXOGUI.setWinButton(4);
            GameXOGUI.setWinButton(6);
        }

    }
}




















