/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package raz.games.tic_tac_toe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author razvan
 */
public class GameXOAI {

    //The Matrix of the Game
    public static final byte mark_x = GameXOLogic.mark_x;
    public static final byte mark_o = GameXOLogic.mark_o;
    public static final byte mark_neutral = GameXOLogic.mark_neutral;
    private byte[][] initialState = null;
    private byte mark_AI = 0;
    private byte mark_ENEMY = 0;
    //List<byte[][]> allSolutions = null;

    /**
     * metoda care initializeaza AI-ul cu o stare si cu un jucator
     */
    public void initStateAI(byte[][] matrixState, byte markAI) {
        System.out.println("====================================================================================================");
        this.initialState = matrixState;
        showMatrix(this.initialState);
        this.mark_AI = markAI;
        System.out.println("COMPUTER:" + this.mark_AI);
        if (markAI == mark_x) {
            //daca AI-ul este cu "X", atunci adversarul este cu "O"
            this.mark_ENEMY = mark_o;
        } else if (markAI == mark_o) {
            this.mark_ENEMY = mark_x;
        }
        System.out.println("PLAYER:" + this.mark_ENEMY);
    //this.generateAllSolutions(matrixState);
    }

    /**
     * Metoda care genereaza toate solutiile posibile pornind de la o stare
     */
//    private void generateAllSolutions(byte[][] matrixState) {
//        allSolutions = new ArrayList();
//
//        for (int i = 0; i <= 2; i++) {
//            for (int j = 0; j <= 2; j++) {
//                byte[][] oneSolution = new byte[3][3];
//                System.out.println("Generating COMPUTER for <<<<< " + "[" + i + "," + j + "]");
//                System.out.println("Input Matrix:");
//                showMatrix(matrixState);
//                oneSolution = generateOneSolution(matrixState, i, j, this.mark_AI);
//                System.out.println("COMPUTER MOVE >>>>>");
//                if (oneSolution != null) {
//                    showMatrix(oneSolution);
//                    allSolutions.add(oneSolution);
//                }
//            }
//        }
//
//    }
    /**
     *Metoda care genereaza o singura solutie 
     * @param matrix - matricea,
     * @param linie, coloana - linia si coloana unde sa marcheze o celula cu "mark_AI"
     * @param marker - se marcheaza cu "mark_x" sau "mark_o", daca AI-ul este cu "X" sau "O"
     * daca matrix[i][j] = mark_neutral, adica este gol, se poate genera solutie
     * daca nu se intoarce null
     */
    public static byte[][] generateOneSolution(byte[][] matrix, int linie, int coloana, byte marker) {
        //Here I "clone" the matrix in order to have pass by value
        byte[][] result = new byte[3][3];
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                result[i][j] = matrix[i][j];
            }
        }

        if (linie < 3 && coloana < 3) {
            if (result[linie][coloana] == mark_neutral) {
                //spatiul este liber deci il pot marca
                result[linie][coloana] = marker;
                //showMatrix(matrix);
                return result;
            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    private byte[][] findSolution(byte[][] initialState) {
        byte[][] result = null;
        List<byte[][]> allAISolutions = generateAllSolutions(initialState, this.mark_AI);
        if (allAISolutions == null || allAISolutions.size() == 0) {
            throw new IllegalStateException("All Possible Solutions is NULL or EMPTY");
        } else {
            if (allAISolutions.size() == 1) {
                /**
                 * Pornind de la starea initiala initialState, s-a putut genera doar o solutie posibila
                 * pentru AI, deci o intorc!
                 * Conditie de STOP pentru Recursivitate
                 */
                System.out.println("STOP condition of Recursion: allAISolutions == 1");
                return allAISolutions.get(0);
            } else {
                /**
                 * Verific daca vreuna din solutiile generate este imediat castigatoare
                 */
                byte[][] newInitialState = initialState;
                int index = 0;
                for (byte[][] oneSolution : allAISolutions) {

                    int rez = GameXOLogic.isGameWon(oneSolution);
                    if (rez > 0) {
                        //oneMatrix is the first winner solution, and the BEST SOLUTION, return it!
                        /**
                         * Avem o solutie castigatoare in lista, deci e cea mai buna solutie, asa ca o intorc
                         * Conditie de STOP in Recursivitate
                         */
                        System.out.println("STOP condition of Recursion: am gasit solutie imediat castigatoare!");
                        return oneSolution;
                    } else {
                        /**
                         * Daca solutia nu e castigatoare, verifica daca nu cumva e imediat pierzatoare, 
                         * caz in care trec la urmatoarea 
                         */
                        boolean badSolution = false;
                        List<byte[][]> allPlayerSolutions = generateAllSolutions(oneSolution, this.mark_ENEMY);
                        for (byte[][] onePlayerSolution : allPlayerSolutions) {
                            int rezPlayer = GameXOLogic.isGameWon(onePlayerSolution);
                            if (rezPlayer > 0) {
                                /* Pentru solutia oneSolution, jucatorul poate sa faca o mutare imediat castigatoare
                                 * deci e o solutie proasta
                                 */
                                badSolution = true;
                                break;// 

                            }
                        }
                        if (badSolution) {
                            /* oneSolution is a bad solution
                             * continua cu urmatoarea
                             */
                            index++;
                            continue;
                        } else {
                            /* oneSolution este o solutie pt care nu exista nicio posibila mutare a PLAYER-ului,
                             * astfel incat acesta sa castige. 
                             * Deci o aleg pe aceasta, iar aceasta devine initialState is se apeleaza recursiv metoda
                             */
                            newInitialState = oneSolution;
                        }
                    }
                }//end For(oneSolution)
                /*
                 * dar newInitialState nu are voie sa difere cu mai mult de o diferenta de this.initialState
                 */
                //-------------------
                int countChanges = 0;
                if (newInitialState != null && this.initialState != null) {
                    for (int i = 0; i <= 2; i++) {
                        for (int j = 0; j <= 2; j++) {
                            if (newInitialState[i][j] != this.initialState[i][j]) {
                                countChanges++;

                            }
                        }

                    }
                }
                //-------------------
                if (countChanges == 0) {
                    // intra in recursie
                    return findSolution(newInitialState);
                    
                } else {
                    //nu intra in recursie
                    return newInitialState;
                }

            }
        }
    //return result;
    }

    private byte[][] getFirstAvailableSolution() {
        byte[][] result = null;
        result = findSolution(this.initialState);
        System.out.println("***********************************");
        showMatrix(result);
        return result;
    }

    /**
     * Metoda asta intoarce prima solutie posibila
     * daca nu exista niciuna , intoarce null
     */
//    private byte[][] getFirstAvailableSolution() {
//        byte[][] result = null;
//        List<byte[][]> allAISoultions = generateAllSolutions(this.initialState, this.mark_AI);
//        if (allAISoultions != null && allAISoultions.size() > 0) {
//            //1.iterez prin lista <<allSolutions>>
//            for (byte[][] oneMatrix : allAISoultions) {
//                //2.verific daca solutia e castigatoare
//                int rez = GameXOLogic.isGameWon(oneMatrix);
//                if (rez > 0) {
//                    //oneMatrix is the first winner solution, and the BEST SOLUTION, return it!
//                    return oneMatrix;
//                }
//
//            }
//            /**
//             * 3. Daca am iesit din ciclul for, si nici o solutie nu a fost castigatoare din prima;
//             * Generez pentru fiecare solutie posibila in parte, lista cu toate solutiile posibile ale inamicului
//             */
//            int indexMySolutions = 0;
//            for (byte[][] oneSolution : allAISoultions) {
//                List<byte[][]> allEnemyMoves = generateAllSolutions(oneSolution, this.mark_ENEMY);
//                /**
//                 * 4. Iterez prin lista de solutii generate pentru adversar si aplica isGameWon().
//                 * Daca asta imi intoarce true, inseamnca ca daca fac mutarea asta, dusmanul castiga,
//                 * asa ca trebuie sa scot solutia din lista cu solutii posibile
//                 */
//                boolean badSolution = false;
//                for (byte[][] oneEnemySolution : allEnemyMoves) {
////                    System.out.println("Checking for isGameWon:");
////                    showMatrix(oneEnemySolution);
//                    int rez = GameXOLogic.isGameWon(oneEnemySolution);
//                    if (rez > 0) {
//                        /**
//                         * pentru soultia i, s-a gasit o posibila mutare castigatoare a inamicului
//                         */
//                        /**
//                         * A)scoate solutia din lista allSolutions
//                         * List<byte[][]> removeMatrixFromList(List<byte[][]> matrixList, byte[][] subject)
//                         */
//                        /**
//                         * B) nu intoarce solutia
//                         */
//                        badSolution = true;
//
//
//                    }
//                }
//                if (badSolution) {
//                    indexMySolutions++;
//                    System.out.println("Found BAD SOLUTION:" + indexMySolutions);
//                    continue;
//
//                } else {
//                    return allAISoultions.get(indexMySolutions);
//                }
//
//            }
//
//            //Just retrun the 1st valid solution
//            return allAISoultions.get(0);
//        }
//
//        return result;
//    }
    /**
     * Pentru fiecare posibila solutie(solutie valida) se genereaza lista de solutii posibile ale adversarului
     * @param oneValidSolution  - o solutie valida
     * @param marker - marker-ul "dusmanului"
     * @return the list of all posible enemy moves
     */
    private static List<byte[][]> generateAllSolutions(byte[][] oneValidSolution, byte marker) {
        System.out.println("Generating All Solutions for Marker:" + marker);
        System.out.println("Input Matrix:");
        showMatrix(oneValidSolution);
        List<byte[][]> result = null;
        result = new ArrayList();

        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                byte[][] oneSolution = null;
                oneSolution = GameXOAI.generateOneSolution(oneValidSolution, i, j, marker);
                if (oneSolution != null) {
                    result.add(oneSolution);
                }
            }
        }
        System.out.println("All Possible Solution List:");
        showList(result);
        return result;
    }

    /**
     * Metoda asta intoarce indecsii solutiei valide alese.
     * Am nevoie de indecsi pentru a putea marca gameMatrix si astfel sa setez butonul
     * @param [i][j]
     */
    public int[] getSolutionIndexes() {
        int[] rezult = new int[2];
        byte[][] firstValidSolution = this.getFirstAvailableSolution();
        int countChanges = 0;
        if (firstValidSolution != null && this.initialState != null) {
            for (int i = 0; i <= 2; i++) {
                for (int j = 0; j <= 2; j++) {
                    if (firstValidSolution[i][j] != this.initialState[i][j]) {
                        countChanges++;
                        rezult[0] = i;
                        rezult[1] = j;
                    }
                }

            }
            if (countChanges > 1) {
                throw new IllegalStateException("Am gasit mai mult de o diferenta intre firstValidSolution si initialState!");
            } else {
                return rezult;
            }

        } else {
            throw new IllegalStateException("firstValidSolution or initialState is NULL");
        }


    }

    public static void showMatrix(byte[][] matrix) {
        if (matrix != null) {
            int xSize = matrix.length;
            for (int i = 0; i < xSize; i++) {
                byte [] temp =  matrix[i];
                int ySize = temp.length;
                for (int j = 0; j < ySize; j++) {
                    System.out.print(/*"[" + i + "][" + j + "]=" +*/matrix[i][j]);
                }
                System.out.println();
            }
            System.out.println();
        } else {
            System.out.println("Matrix is null");
        }
    }

    private static void showList(List<byte[][]> list) {
        if (list == null || list.size() == 0) {
            System.out.println("List is either Null or Empty!");
        } else {
            int len = list.size();
            if (len > 0) {
                int index = 0;
                while (index < len) {
                    System.out.println("Item:" + index);
                    byte[][] oneMatrix = list.get(index);
                    index++;
                    showMatrix(oneMatrix);
                }
            } else {
                System.out.println("List is Empty");
            }
        }

    }

    public void showAllPossibleSolutionList() {
        //this.showList(this.allSolutions);
    }
    ;
}
