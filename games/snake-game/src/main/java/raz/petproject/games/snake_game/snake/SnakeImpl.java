package raz.petproject.games.snake_game.snake;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by razvan on 05/03/2019.
 */
public class SnakeImpl implements Snake {

    private Direction direction;

    int length;

    Pair<Integer, Integer> headPosition;//initialHeadPosition


    //after you calculate current coordinates, cache them, so you can use them when you want to compute the next move
    List<Pair<Integer, Integer>>  previousCoordinates;

    List<Pair<Integer, Integer>> snakeCoordinates;

    public SnakeImpl(int length, Pair<Integer, Integer> headPosition, Direction direction) {
        this.length = length;
        this.headPosition = headPosition;
        this.direction = direction;

        this.computeSnakeCoordinatesOnInit();

        this.previousCoordinates = new ArrayList<>(this.getCoordinates());

    }

    //Package Visibility so I can test
    private void computeSnakeCoordinatesOnInit(){
        snakeCoordinates = new ArrayList<>();

        //1. Agauga capul
        snakeCoordinates.add(headPosition);

        //2. Deseneaza corpul in functie de direction
        int snakeElementCount = 2;
        Pair<Integer, Integer> bodyCoordinate;
        while (snakeElementCount<=length){
            int elementAfterHeadOffset = snakeElementCount -1;
            bodyCoordinate = this.direction.getBodyCoordinateInReferenceToHead(headPosition, elementAfterHeadOffset);
            snakeCoordinates.add(bodyCoordinate);
            snakeElementCount++;
        }

    }

    public void moveSnake(Direction currentDir){
        if(snakeCoordinates!=null)
        snakeCoordinates.clear();

        //1. Muta capul
        headPosition = currentDir.advanceHeadCoordinate(headPosition);
        snakeCoordinates.add(headPosition);

        //Gatul se muta in locul in care a fost capul
        //deci de la cap pina la pen-ultimul element
        for(int i=0; i<=length-2; i++){
            snakeCoordinates.add(previousCoordinates.get(i));
        }
    }

    @Override
    public List<Pair<Integer, Integer>> getCoordinates() {
        if(snakeCoordinates==null){
            throw new IllegalStateException("Pozitia trebuia calculata, Programming error, Bug in logica..");
        }
        return snakeCoordinates;
    }

    private void computeCoordinates(){}

    @Override
    public int getCurrentSize() {
        return length;
    }
}
