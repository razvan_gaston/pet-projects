package raz.petproject.games.snake_game;

import javafx.util.Pair;
import raz.petproject.games.snake_game.snake.Direction;
import raz.petproject.games.snake_game.snake.Snake;
import raz.petproject.games.snake_game.snake.SnakeImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by razvan on 02/02/2019.
 */
public class SnakeLogic extends SnakeGameBase implements SnakeGame<Integer[]>{

    static final int GAME_STATE_READY_TO_START_NEW_GAME = 0;


    private final Integer[][] gameSpace;

    Snake snake;


    public SnakeLogic(int matrixSize) {
        super(matrixSize);
        gameSpace = new Integer[gameSize][gameSize];
        MatrixUtil.initMatrix(gameSpace, GameCell.EMPTY.value);
    }



//    @Override
//    public int[][] getGameSpace() {
//        return gameSpace;
//    }

    //Package protected so I can Unit test

    @Override
    public Integer[][] getGameSpace() {
        return gameSpace;
    }

    /**
     * - When the game Begins, I need to place the snake_game in the middle
     */
    @Override
    public void initSnakePosition() {

        snake = new SnakeImpl(Snake.gameStartSize, getSnakeHeadCoordinatesAtGameStart().get(0), Direction.RIGHT);
    }

    @Override
    public List<Pair<Integer, Integer>> getSnakeHeadCoordinatesAtGameStart() {
        int initialSnakePos = getSnakeHeadPosAtGameStart();
        List<Pair<Integer, Integer>> result = new ArrayList<>();
        Pair<Integer, Integer> headPos = new Pair<>(initialSnakePos, initialSnakePos);
        result.add(headPos);
        return result;
    }


    @Override
    public List<Pair<Integer, Integer>> getSnakeCoordinates(){
        return snake.getCoordinates();
    }

    @Override
    public void setSnakeIntoGameSpace() {

    }

    @Override
    public Snake getSnake() {
        return null;
    }

    @Override
    public void nextFrame() {

    }


}
