package raz.petproject.games.snake_game;

import raz.petproject.games.snake_game.snake.Snake;
import raz.petproject.games.snake_game.snake.SnakeUniDim;

import java.util.Arrays;
import java.util.List;

public class SnakeLogicUniDim extends SnakeGameBase implements SnakeGame<Integer> {

    Snake uniDimSnake;

    private final Integer[] gameSpace;

    public SnakeLogicUniDim(int gameSize) {
        super(gameSize);
        gameSpace = new Integer[gameSize];
        MatrixUtil.initMatrix(gameSpace, GameCell.EMPTY.value);
    }

    @Override
    public List getSnakeHeadCoordinatesAtGameStart() {
        int initialSnakePos = getSnakeHeadPosAtGameStart();
        return Arrays.asList(initialSnakePos);
    }

    @Override
    public Integer[] getGameSpace() {
        return gameSpace;
    }

    @Override
    public void initSnakePosition() {

        uniDimSnake = new SnakeUniDim(getSnakeHeadCoordinatesAtGameStart());

        setSnakeIntoGameSpace();
    }

    @Override
    public List getSnakeCoordinates() {
        return uniDimSnake.getCoordinates();
    }


    @Override
    public void setSnakeIntoGameSpace() {
        for(Object oneSnakeIdx : getSnake().getCoordinates()){
            gameSpace[ (int) oneSnakeIdx ] = GameCell.SNAKE.value;
        }
    }

    @Override
    public Snake getSnake() {
        return uniDimSnake;
    }

    @Override
    public void nextFrame() {
        //1. increase frame number
        this.currentFrame++;

        //2 Update snake_game position in functie de directie
        //in fiecare frame/la fiecare frameUpdate sarpele avanseaza cate o pozitie/un patratel/o celula  in directia apasata de user

        //3. daca mancarea a fost mancata , pune mancare intr-un nou loc
        //FIXME: - nu as face/amesteca asta pe frameUpdate....
        //ci e oarecum independent de frameUpdate.
        //pe metoda de foodEaten(cand sarpele manca o mancare/ajunge la mancare)
        // dupa ce se mananca mancarea currenta, sa se puna alta
        //TODO: dar cum implementezi/verifici asta? - foodEaten(cand sarpele manca mancarea/ajunge la mancarea curenta)

        //vezi daca sarpele a ajuns la marginea gameSpace-ului

        //vezi daca sarpele s-a bag in el insusi

        //TODO: SnakeUni Dim nu te ajuta la cum sa faci sarpele sa se miste in plan biDiimensional,
        //ci mai mult te incurca, asa ca ar fi bine sa renunit
        //in schimb te ajuta la chestiile de gameController, frmae, UI, mancare ?!


    }


}
