package raz.petproject.games.snake_game.snake;

import java.util.List;

public interface Snake {

    int gameStartSize = 4;

    List getCoordinates();

    int getCurrentSize();

    // - Un sarpe de fapt nu are Body !!!
    // Body, este iluzia realizata de GUI ca sa inteleaga jucatorul, Doar jucatorul vede/percepe Sarpele ca avand body
    /*
    - de fapt un sarpe este reprezentat intern ca
    1/ Head Coordinates
    2/Length

    3/ legea de miscare descrisa de cap, pe care body-ul trebuie sa o repete
    (- e ca si cand ai clona capul)
    trebuie sa tin minte traiectoria capului, traseul pe care a mers capul,

    - sau si mai simplu, trebuie sa tin minte toate schimbarile de directie!!
    - Am nevoie de o structura formata din:
        - pozitia curenta ca capului,
        - si directia : up/down, left/right
    - in structura asta, introduc o noua valoare DOAR Daca a intervenit o schimbare de directie in frameul current fata de ultima directie
    * */


     void moveSnake(Direction currentDir);
}
