package raz.petproject.games.snake_game;


import raz.petproject.games.snake_game.snake.Snake;

import java.util.List;

//https://stackoverflow.com/a/2924453/1864614
public interface SnakeGame<E> {

    public E[] getGameSpace();

    void initSnakePosition();

    /**
     * Get the current Coordinates of the snake_game
     * */
    List getSnakeCoordinates();

    void setSnakeIntoGameSpace();

    Snake getSnake();

    /**
     * Am introdus notiunea de Game Frame/ Game heart Beat
     * - frameul 0, este snapShotul de initializare, cu GameSpace-ul curat, sarpele in pozitia initiala de start, etc...
     * - frame-ul 1, este primul frame al jocului diferit fata de frameul 0/snapShotul de initializare
     * - la fiecare frame trebuie
     * > sa updatez/recalculez pozitia sarpelui in functie de directie/tasta apasata
     * > sa pun o noua mancare daca jucatorul a mancat mancare
     * > am nevoie de un timer la 1 secuda care sa tot apeleze metoda asta de doFrameUpdates
     *
     * FIXME:
     * - daca as fi sa scriu acum un test, pentru ca inca nu am notiunea de directie, frame1 este egal cu frame0, adica deocamdata nu se intampla nimic cu sarpele ca nu am notiunea de directie
     * */
    int getCurrentFrame();

    //metoda asta va fi apelata dintr-un java timer la fiecare 1 secounda
    //si avanseaza jocul la next frame
    //void advanceToNextFrame();
    void nextFrame();

    //Metoda asta va fi chemata din UI
    //M-am razgandit, startGame sa fie chemata din alta clasa, ceva gen GameController
    //GameController va avea un SnakeGame logic si-i va spune ce sa faca, va avea timerul si la fiecare secunda
    //va apela SnakeGame logic, nextFrame
    //void startGame();
}
