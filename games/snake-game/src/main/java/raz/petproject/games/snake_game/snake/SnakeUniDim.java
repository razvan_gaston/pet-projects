package raz.petproject.games.snake_game.snake;

import raz.petproject.games.snake_game.snake.Snake;

import java.util.List;

public class SnakeUniDim implements Snake {



    //Sarpele este dor o lista inlantuita de coordonate care au alta valoare decat EmptySpace
    List<Integer> snakeCoordinates; /*= new ArrayList<>();*/

    public SnakeUniDim(List<Integer> initialSnakeCoordinates){
        snakeCoordinates = initialSnakeCoordinates;
    }

    @Override
    public List getCoordinates() {
        return snakeCoordinates;
    }

    @Override
    public int getCurrentSize() {
        return snakeCoordinates.size();
    }

    @Override
    public void moveSnake(Direction currentDir) {

    }
}
