package raz.petproject.games.snake_game;

public enum GameCell {


    EMPTY(0),

    /*inca nu am nevoia de distinctie intre SNAKE Body si head*/
    SNAKE(1),

    FOOD(2);

    public final int value;

    GameCell(int val) {
        value = val;
    }


}
