package raz.petproject.games.snake_game;

/**
 * Created by razvan on 02/02/2019.
 */
public class MatrixUtil {


    public static void printMatrixLineCol(int[][] matrix) {
        for (int y = 0; y < matrix.length - 1; y++) {
            for (int x = 0; x < matrix.length - 1; x++) {
                System.out.println("[" + x + "][" + y + "]=" + matrix[x][y]);
            }
        }
    }


    public static void printMatrixLineCol2(Integer[][] matrix) {
        StringBuilder result = new StringBuilder();
        StringBuilder col_Y;
        System.out.println("numarul de tabele:" + matrix.length);
        for (int i = 0; i < matrix.length; i++) {
            Integer[] i_mainArr = matrix[i];
            if (i_mainArr != null) {
                System.out.println("Tabelul,i[" + i + "] len:" + i_mainArr.length);
                printArray(i_mainArr);
                col_Y = new StringBuilder();
                for (int j = 0; j < i_mainArr.length; j++) {
                    //col_Y.append("[i"+i+"][j"+j+"]=").append(matrix[i][j]);
                    col_Y.append("[line" + i + "][col" + j + "]=").append(i_mainArr[j]);
                }
                col_Y.append("\n");
                result.append(col_Y);
            } else {
                System.out.println("Tabelul,i[" + i + "] :" + i_mainArr);
            }
        }
        System.out.println();
        System.out.println(result.toString());
    }


    public static void printArray(Integer[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print("[" + i + "]=" + array[i]);
        }


    }

    public static void showMatrix(Integer[][] matrix) {
        if (matrix != null) {
            int xSize = matrix.length;
            for (int i = 0; i < xSize; i++) {
                Integer[] temp = matrix[i];
                int ySize = temp.length;
                for (int j = 0; j < ySize; j++) {
                    System.out.print(/*"[" + i + "][" + j + "]=" +*/matrix[i][j]);
                }
                System.out.println();
            }
            System.out.println();
        } else {
            System.out.println("Matrix is null");
        }
    }

    /**
     * Asserts that <b>all the Matrix values</b> are equal to the given value
     */
    public static void assertMatrixValues(Integer[][] matrix, int val) {
        for (int i = 0; i < matrix.length; i++) {
            Integer[] i_array = matrix[i];
            for (int j = 0; j < i_array.length; j++) {
                if (matrix[i][j] != val) {
                    throw new IllegalStateException("[" + i + "][" + j + "]=" + matrix[i][j] + " is different than expected:" + val);
                }
            }
        }
    }

    public static void assertMatrixValues(Integer[] matrix, int val) {
        for (int j = 0; j < matrix.length; j++) {
            if (matrix[j] != val) {
                throw new IllegalStateException("[" + j + "]=" + matrix[j] + " is different than expected:" + val);
            }
        }
    }

    /**
     * Initializez the Matrix with the given value
     */
    public static void initMatrix(Integer[][] matrix, int val) {
        if (matrix == null) {
            throw new IllegalArgumentException("The matrix can not be null");
        }
        int numberOfLines = matrix.length;
        for (int i = 0; i < numberOfLines; i++) {
            Integer[] colonsAtLine = matrix[i];
            int len = colonsAtLine.length;
            for (int j = 0; j < len; j++) {
                colonsAtLine[j] = val;
            }
        }
    }

    public static void initMatrix(Integer[] matrix, int val) {
        if (matrix == null) {
            throw new IllegalArgumentException("The matrix can not be null");
        }
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = val;
        }
    }


}
