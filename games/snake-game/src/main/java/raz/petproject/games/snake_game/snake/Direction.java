package raz.petproject.games.snake_game.snake;

import javafx.util.Pair;

/**
 * Created by razvan on 05/03/2019.
 */
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;


    /**
     * Returns a new Coordinate which has the correct adjustment based on DIRECTION
     * */
    Pair<Integer, Integer> advanceHeadCoordinate(Pair<Integer, Integer>  input){
//        Pair<Integer, Integer> result = null;
//        if(this==UP){
//            int i = input.getKey();
//            result= new Pair<>(i-1, input.getValue());
//        }else if(this==DOWN){
//            int i = input.getKey();
//            result= new Pair<>(i+1, input.getValue());
//        } else if(this==RIGHT){
//            int j = input.getValue();
//            result= new Pair<>(input.getKey(), j+1);
//        }else if(this==LEFT){
//            int j = input.getValue();
//            result= new Pair<>(input.getKey(), j-1);
//        }
//        return result;
        return advanceHeadCoordinate(input, 1);
    }

    /**
     * Returns a new Coordinate which has the correct adjustment based on DIRECTION
     * */
    Pair<Integer, Integer> advanceHeadCoordinate(Pair<Integer, Integer>  input, int offSet){
        Pair<Integer, Integer> result = null;
        if(this==UP){
            int i = input.getKey();
            result= new Pair<>(i-offSet, input.getValue());
        }else if(this==DOWN){
            int i = input.getKey();
            result= new Pair<>(i+offSet, input.getValue());
        } else if(this==RIGHT){
            int j = input.getValue();
            result= new Pair<>(input.getKey(), j+offSet);
        }else if(this==LEFT){
            int j = input.getValue();
            result= new Pair<>(input.getKey(), j-offSet);
        }
        return result;
    }


    Pair<Integer, Integer> getBodyCoordinateInReferenceToHead(Pair<Integer, Integer>  head, int offSet){
        Pair<Integer, Integer> result = null;
        if(this==UP){
            int i = head.getKey();
            result= new Pair<>(i+offSet, head.getValue());
        }else if(this==DOWN){
            int i = head.getKey();
            result= new Pair<>(i-offSet, head.getValue());
        } else if(this==RIGHT){
            //If direction is RIGHT, that means the body element is Left of the Head
            int j = head.getValue();
            result= new Pair<>(head.getKey(), j-offSet);
        }else if(this==LEFT){
            int j = head.getValue();
            result= new Pair<>(head.getKey(), j+offSet);
        }
        return result;
    }

}
