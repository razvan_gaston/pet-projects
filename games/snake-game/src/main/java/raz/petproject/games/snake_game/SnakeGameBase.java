package raz.petproject.games.snake_game;

import java.util.List;

public abstract class SnakeGameBase {

    protected final int gameSize;

    protected int currentFrame = 0;

    public SnakeGameBase(int gameSize) {
        this.gameSize = gameSize;
    }


    /**
     * Punem sarpele la Jumatea spatiului cand incepe jocul
     */
    protected int getSnakeHeadPosAtGameStart() {
        return (gameSize / 2) - 1;
    }

    public abstract List getSnakeHeadCoordinatesAtGameStart();

    public int getCurrentFrame() {
        return currentFrame;
    }
}
