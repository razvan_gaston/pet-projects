package raz.petproject.games.snake_game;

import org.junit.Test;

/**
 * Created by razvan on 2/8/19.
 */
public class MatrixUtilShould {


    @Test(expected = IllegalStateException.class)
    public void assertMatrixValues_negative_scenarie() throws Exception {
        Integer[][] matrix = new Integer[2][2];
        MatrixUtil.initMatrix(matrix,0);

        MatrixUtil.showMatrix(matrix);
        matrix[1][1] = 1;
        MatrixUtil.showMatrix(matrix);

        MatrixUtil.assertMatrixValues(matrix, 1);
    }

}