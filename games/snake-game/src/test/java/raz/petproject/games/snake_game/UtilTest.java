package raz.petproject.games.snake_game;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by razvan on 02/02/2019.
 */
public class UtilTest {


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void understandingArrays() throws Exception {

        int mySingleValue = 1;
        //este echivalent cu
        int[] singleValArr = new int[1];
        singleValArr[0] = 1;
    }


    @Test
    public void printMatrixLineCol() throws Exception {
        SnakeLogic snakeGame = new SnakeLogic(10);

        MatrixUtil.printMatrixLineCol2(snakeGame.getGameSpace());
    }

    @Test
    public void understandingMatrixStructure() throws Exception {

        Integer[][] gameSpaceA = new Integer[2][10];

        MatrixUtil.printMatrixLineCol2(gameSpaceA);

    }

    @Test
    public void understandingMatrixStructure_second_val_unspecified() throws Exception {

        Integer[][] gameSpaceB = new Integer[2][];

        MatrixUtil.printMatrixLineCol2(gameSpaceB);

    }


}