package raz.petproject.games.snake_game;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by razvan on 2/8/19.
 */
public class UniDimSnakeGameLogicShould {

    int gameSize = 10;

    SnakeGame<Integer> snakeGame = new SnakeLogicUniDim(10);

    @Test
    public void before_game_start_check_game_space_is_empty() {

        assertEquals("check game size", gameSize, snakeGame.getGameSpace().length);

        MatrixUtil.printArray(snakeGame.getGameSpace());

        MatrixUtil.assertMatrixValues(snakeGame.getGameSpace(), GameCell.EMPTY.value);

    }


    @Test
    public void before_game_start_check_snake_position() {
        snakeGame.initSnakePosition();

        //Sarpele ar trebui sa fie la jumate
        assertEquals("Verific Coordonatele Snake", ((SnakeGameBase)snakeGame).getSnakeHeadCoordinatesAtGameStart(), snakeGame.getSnakeCoordinates());

        MatrixUtil.printArray(snakeGame.getGameSpace());

        //Verific ca sarpele a fost inserat in GameSpace
        List<Integer> snakeCoordinates = ((SnakeGameBase)snakeGame).getSnakeHeadCoordinatesAtGameStart();
        //TODO: deocamdata sarpele are size 1
        int temp = snakeGame.getGameSpace()[snakeCoordinates.get(0)];
        assertEquals(GameCell.SNAKE.value, temp);
    }

    //TODO 1 , next Introdu notiunea de Direction in care se misca sarpele (in functie de ce sageata/Tasta o apasat userul)
    // In functie de directie, la urmatoarea Frecventa, recalculeaza pozitia sarpelui


    @Test
    public void doTheThingsRequiredForNextFrame() throws Exception {
        //snakeGame.startGame();
        snakeGame.initSnakePosition();

        snakeGame.nextFrame();

        MatrixUtil.printArray(snakeGame.getGameSpace());
    }

}