package raz.petproject.games.snake_game.snake;

import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by razvan on 05/03/2019.
 */
public class SnakeImplTest {
    @Test
    public void moveSnake() throws Exception {

    }

    @Test
    public void getCoordinates() throws Exception {

    }

    @Test
    public void generateSnakeCoordinates_On_game_init() throws Exception {

        int snakeLen = 4;

        int iHead = 6;
        int jHead = 5;

        //Given
        Pair<Integer, Integer> headPosition = new Pair<>(iHead, jHead);

        Snake snake = new SnakeImpl(snakeLen, headPosition, Direction.RIGHT);

        //when
        List<Pair<Integer, Integer>> snakeCoordinates = snake.getCoordinates();


        //Assert
        List<Pair<Integer, Integer>> expected = new ArrayList<>();

        expected.add(headPosition);
        expected.add(new Pair<>(iHead, jHead - 1));
        expected.add(new Pair<>(iHead, jHead - 2));
        expected.add(new Pair<>(iHead, jHead - 3));

        assertEquals(expected, snakeCoordinates);

    }

    @Test
    public void move_up() throws Exception {

        int snakeLen = 4;

        int iHead = 6;
        int jHead = 5;

        //Given
        Pair<Integer, Integer> headPosition = new Pair<>(iHead, jHead);

        Snake snake = new SnakeImpl(snakeLen, headPosition, Direction.RIGHT);
        //Asta a fost asertat deja in testul de mai sus Assert
        List<Pair<Integer, Integer>> expectedObs = new ArrayList<>(snakeLen);

        expectedObs.add(headPosition);
        expectedObs.add(new Pair<>(iHead, jHead - 1));
        expectedObs.add(new Pair<>(iHead, jHead - 2));
        expectedObs.add(new Pair<>(iHead, jHead - 3));

        assertEquals(expectedObs, snake.getCoordinates());


        //WHEN
        snake.moveSnake(Direction.UP);


        //ASSERT
        List<Pair<Integer, Integer>> expected = new ArrayList<>(snakeLen);

        expected.add(new Pair<>(5, 5));
        expected.add(new Pair<>(6, 5));
        expected.add(new Pair<>(6, 4));
        expected.add(new Pair<>(6, 3));


        assertEquals(expected, snake.getCoordinates());
    }


}