package raz.petproject.games.snake_game;


import javafx.util.Pair;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SnakeLogicShould {


    int gameSize = 10;


//      What is the simplest valid(correctly identified, specified, and isolated) behaviour?
//    Go and implement that


    /**
     * Check just before start, game set-up
     * <p>
     * Check the game Start Position is correct
     * - game space is clean
     * - snake_game is 3 cels and positioned correctly
     * - adica la mijloc - pe linia a 5
     * - iar pe linia a 5-a la mijlocul liniei, adica incepand de la coloana a 4. ar veni 4,5,6 si raman libere: 7,8,9,10 deci 4 cells. Capul/orientarea sa fie la dreapta, unde sunt 4 celule libere pina in zid
     * - sarpele initial tre sa fie de marime 3 patratele/cells
     */
    public void justBeofreStartingTheGameTheGameMatrixShouldBeClean() throws Exception {

    }

    @Test
    public void before_game_start_check_game_space_is_empty() {


        SnakeLogic gameLogic = new SnakeLogic(gameSize);

        Integer[][] gameSpace = gameLogic.getGameSpace();

        assertEquals("check game size", gameSize, gameSpace.length);
        MatrixUtil.showMatrix(gameSpace);

        MatrixUtil.assertMatrixValues(gameSpace, GameCell.EMPTY.value);
    }


    @Test
    public void before_game_start_check_snake_position() {

        SnakeLogic gameLogic = new SnakeLogic(gameSize);

        gameLogic.initSnakePosition();

        List<Pair<Integer,Integer>> snakeCoordinates = gameLogic.getSnakeCoordinates();


        //To Start Simple, sa zicem ca Snake-ul e format doar din HEAD - one cell only

        //Do I need a get snake_game function ?! intoarce coordonatele snake_game-ului
        /*
        FIXME - Ideea Sarpe UniDimensional
        - To get the concepts right, and to simply the situation/logic, and then to extrapolate, am urmatoarea idee

        - sa ma gandesc la un Sarpe UniDimensional
        * */


        //assertEquals();

    }


}