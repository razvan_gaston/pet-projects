package raz.kata.game_of_life;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameMatrixCsvConverter extends SimpleArgumentConverter {

    private Class calzz = null;

    /* How many times has this Converter Instance been invoked in order to convert the given
    input Source(String)
    to the desired Parameter Type
    * */
    private int invocationCount = 0;

    Cell[][] gameMatrix = new Cell[3][];

    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {


        assertEquals(String.class, source.getClass());
        assertEquals(getTargetClass(), targetType);

        //https://www.baeldung.com/java-regexp-escape-char
        String[] oneRowValues = source.toString().split(Pattern.quote("|"));

        System.out.println(this.hashCode() + "invocationCount:" + invocationCount);


        List<String> rowValues = Stream.of(oneRowValues)
                .map(s -> s.trim())
                .collect(Collectors.toList());

        System.out.println(rowValues);

        Cell[] oneMatrixRow = new Cell[rowValues.size()];
        Cell cell = null;


        for (int idx = 0; idx <= rowValues.size() - 1; idx++) {
            cell = new Cell(
                    CellState.fromValue(rowValues.get(idx)),
                    invocationCount,
                    idx,
                    gameMatrix
            );
            oneMatrixRow[idx] = cell;
        }

        gameMatrix[invocationCount] = oneMatrixRow;

        invocationCount++;
        return gameMatrix;
    }

    /**
     * What is very interesting and new for me, is how I load a Class Object which is an Array of Object
     * calzz = Class.forName("[[Lraz.kata.game_of_life.Cell;"); --> WOW!
     */
    private Class getTargetClass() {
        if (calzz == null) {
            try {
                calzz = Class.forName("[[Lraz.kata.game_of_life.Cell;");
                //https://www.geeksforgeeks.org/array-primitive-type-object-java/
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return calzz;
    }
}
