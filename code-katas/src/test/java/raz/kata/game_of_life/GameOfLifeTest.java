package raz.kata.game_of_life;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import raz.kata.util.MatrixUtil;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class GameOfLifeTest {

    GameOfLife gameOfLife;

    GameOfLifeMatrixCsvConverter csvMatrixLoader = new GameOfLifeMatrixCsvConverter();


    Path testSrcResources = Paths.get("src", "test", "resources");

    @BeforeEach
    void setUp() {
        gameOfLife = new GameOfLife(3, 3);
    }

    @Test
    void game_matrix_initialization() {

        MatrixUtil.showMatrix(gameOfLife.getGameMatrix());
    }

    @Test
    void given_theCoordsForLiveCells_initialize_game_matrix_with_given_setUp() throws IOException {

        Collection<RowColPair> liveCells = Arrays.asList(
                new RowColPair(1, 2),
                new RowColPair(2, 1),
                new RowColPair(2, 2));

        gameOfLife.setLiveCells(liveCells);

        Cell[][] gameMatrix = gameOfLife.getGameMatrix();

        MatrixUtil.showMatrix(gameMatrix);

        Path filePath = testSrcResources.resolve("gameMatrixSnapshot.csv");
        Cell[][] expectedMatrix = csvMatrixLoader.loadFromCsv(filePath);

        assertArrayEquals(expectedMatrix, gameMatrix, "game matrices should be equal");
    }

    @Test
    @Disabled("Asta l-am folosit doar sa vad cum merge csvMatrixLoader")
        //@CsvFileSource(resources = "/gameMatrixSnapshot.csv")
    void loadMatrixFromCSV( /*@ConvertWith(GameMatrixCsvConverter.class) Cell[][] gameMatrix*/) throws IOException {

        Path filePath = testSrcResources.resolve("gameMatrixSnapshot.csv");

        Cell[][] gameMatrix = csvMatrixLoader.loadFromCsv(filePath);

        MatrixUtil.showMatrix(gameMatrix);
    }

    @Test
    @Disabled("Asta l-am folosit doar sa vad calss name-ul array-ului  >>>[[Lraz.kata.game_of_life.Cell;<<")
    void type_of_Matrix() {
        gameOfLife = new GameOfLife(3, 3);

        Cell[][] gameMatrix = gameOfLife.getGameMatrix();

        MatrixUtil.showMatrix(gameMatrix);

        System.out.println(" >>>" + gameMatrix.getClass().getName() + "<<");
        System.out.println(" >>>" + gameOfLife.getClass().getName() + "<<");
    }
}
