package raz.kata.game_of_life;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

class GameOfLifeDifferentScenariosTest {

    GameOfLife gameOfLife;

    Path testSrcResources = Paths.get("src", "test", "resources");
    Path testSubDir = testSrcResources.resolve("gameScenarios");

    GameOfLifeMatrixCsvConverter csvMatrixLoader = new GameOfLifeMatrixCsvConverter();

    @Test
    void center_cell_dies_then_corner_lonly_cell_dies() throws Exception {
        Path filePath = testSubDir.resolve("moare_aia_din_mijloc.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);

        gameOfLife.playTheGame();
    }


    @Test
    void test1() throws Exception {
        Path filePath = testSubDir.resolve("mor_alea_din_extremitati_chain_reaction.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);

        gameOfLife.playTheGame();
    }

    @Test
    void test2() throws Exception {
        Path filePath = testSubDir.resolve("4_by_4_full.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);

        gameOfLife.playTheGame();
    }

    @Test
    void test3() throws Exception {
        Path filePath = testSubDir.resolve("5_by_5_full.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);

        gameOfLife.playTheGame();
    }

    @Test
    void test4() throws Exception {
        Path filePath = testSubDir.resolve("5_by_5_oscilator.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);

        gameOfLife.playTheGame();
    }
}
