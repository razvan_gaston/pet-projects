package raz.kata.game_of_life;

import org.junit.jupiter.api.Test;
import raz.kata.util.MatrixUtil;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CellShould {

    Path testSrcResources = Paths.get("src", "test", "resources");
    Path testSubDir = testSrcResources.resolve("getNeighbours");

    GameOfLifeMatrixCsvConverter csvMatrixLoader = new GameOfLifeMatrixCsvConverter();

    GameOfLife gameOfLife;

    @Test
    void get_neighbours_corner_position() throws IOException {
        Path filePath = testSubDir.resolve("cell_0_0_neighbours_corner.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(gameOfLife.getGameMatrix());

        Cell theCell = gameOfLife.getGameMatrix()[0][0];

        Collection<Cell> ngh = theCell.neighbours();

        assertEquals(3, ngh.size());
        for (Cell neighbour : ngh) {
            assertEquals(CellState.DEAD, neighbour.getState());
        }
    }

    @Test
    void get_neighbours_on_the_edge_middle_position() throws IOException {
        Path filePath = testSubDir.resolve("cell_0_1_neighbours_edge_middle.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(gameOfLife.getGameMatrix());

        Cell theCell = gameOfLife.getGameMatrix()[0][1];

        Collection<Cell> ngh = theCell.neighbours();

        assertEquals(5, ngh.size());
        for (Cell neighbour : ngh) {
            assertEquals(CellState.DEAD, neighbour.getState());
        }
    }

    @Test
    void get_neighbours_center_position() throws IOException {
        Path filePath = testSubDir.resolve("cell_1_1_neighbours_matrix_center.csv");

        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);

        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(gameOfLife.getGameMatrix());

        Cell theCell = gameOfLife.getGameMatrix()[1][1];

        Collection<Cell> ngh = theCell.neighbours();

        assertEquals(8, ngh.size());
        for (Cell neighbour : ngh) {
            assertEquals(CellState.DEAD, neighbour.getState());
        }
    }
}
