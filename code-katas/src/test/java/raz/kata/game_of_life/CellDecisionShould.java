package raz.kata.game_of_life;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import raz.kata.util.MatrixUtil;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CellDecisionShould {

    Path testSrcResources = Paths.get("src", "test", "resources");

    Path testSubDir = testSrcResources.resolve("gameRules");

    GameOfLifeMatrixCsvConverter csvMatrixLoader = new GameOfLifeMatrixCsvConverter();

    CellDecision cellDecision;

    GameOfLife gameOfLife;

    @BeforeEach
    void setUp() {
        //gameOfLife = new GameOfLife(3, 3);
    }

    @Test
    void cell_with_1_live_neighbour_dies() throws IOException {
        Path filePath = testSubDir.resolve("cell_0_0_1_live_neighbour.csv");
        //GIVEN - load game of life SnapShot from .csv
        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);
        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(origin);


        Cell cell = gameOfLife.getGameMatrix()[0][0];
        cellDecision = new CellDecision(cell);

        //WHEN We evaluate a cell based on it's current neighbours
        CellState result = cellDecision.outcome();

        //THEN
        assertEquals(CellState.DEAD, result);
    }

    @Test
    void cell_with_2__live_neighbours_lives_on() throws IOException {
        Path filePath = testSubDir.resolve("cell_0_0_2_live_neighbours.csv");
        //GIVEN - load game of life SnapShot from .csv
        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);
        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(origin);


        Cell cell = gameOfLife.getGameMatrix()[0][0];
        cellDecision = new CellDecision(cell);

        //WHEN We evaluate a cell based on it's current neighbours
        CellState result = cellDecision.outcome();

        //THEN
        assertEquals(CellState.ALIVE, result);
    }

    @Test
    void cell_with_3__live_neighbours_lives_on() throws IOException{
        Path filePath = testSubDir.resolve("cell_0_0_3_live_neighbours.csv");
        //GIVEN - load game of life SnapShot from .csv
        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);
        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(origin);


        Cell cell = gameOfLife.getGameMatrix()[0][0];
        cellDecision = new CellDecision(cell);

        //WHEN We evaluate a cell based on it's current neighbours
        CellState result = cellDecision.outcome();

        //THEN
        assertEquals(CellState.ALIVE, result);
    }

    @Test
    void cell_with_4_live_neighbours_must_die() throws IOException{
        Path filePath = testSubDir.resolve("cell_1_1_4_live_neighbours.csv");
        //GIVEN - load game of life SnapShot from .csv
        Cell[][] origin = csvMatrixLoader.loadFromCsv(filePath);
        gameOfLife = new GameOfLife(origin);
        MatrixUtil.showMatrix(origin);


        Cell cell = gameOfLife.getGameMatrix()[1][1];
        cellDecision = new CellDecision(cell);

        //WHEN We evaluate a cell based on it's current neighbours
        CellState result = cellDecision.outcome();

        //THEN
        assertEquals(CellState.DEAD, result);
    }

}
