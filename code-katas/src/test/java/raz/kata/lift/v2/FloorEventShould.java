package raz.kata.lift.v2;

import org.junit.jupiter.api.Test;
import raz.kata.lift.Direction;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FloorEventShould {

    @Test
    void sort_ASC_target_destinations() {
        FloorEvent fromFloor = new FloorEvent(0, Arrays.asList(1, 3, 2));

        assertEquals(Direction.UP, fromFloor.getRequestDirection());

        assertEquals(Arrays.asList(1, 2, 3), fromFloor.getDestinationFloors().collect(Collectors.toList()), "Going UP, so ASC sorted");
    }

    @Test
    void sort_DESC_target_destinations() {
        FloorEvent fromFloor = new FloorEvent(5, Arrays.asList(1, 3, 2));

        assertEquals(Direction.DOWN, fromFloor.getRequestDirection());

        assertEquals(Arrays.asList(3, 2, 1), fromFloor.getDestinationFloors().collect(Collectors.toList()), "Going DOWN, so DESC sorted");
    }

    @Test
    void sort_NoDirection_No_Sort_target_destinations() {

        FloorEvent fromFloor = new FloorEvent(3, Arrays.asList(5, 1, 4));

        assertEquals(Direction.UP, fromFloor.getRequestDirection());

        assertEquals(Arrays.asList(5, 1, 4), fromFloor.getDestinationFloors().collect(Collectors.toList()), "Going UP and DOWN, so just leave them as is");
    }
}
