package raz.kata.lift.v3;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FloorEventLazyTest {

    @Test
    void processFloorRequest() {

        List<Integer> requestedDestinationsAt1stFloor = Arrays.asList(3,4);

        FloorEventLazy firstFloorReq = new FloorEventLazy(1, ()->requestedDestinationsAt1stFloor);

    }
}
