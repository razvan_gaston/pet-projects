package raz.kata.lift;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled("LiftManager was discarded and I re-implemented in  LiftScheduler")
class LiftManagerShould {

    //What is the simplest use-case?

    ILiftManager liftManager = new LiftManager(5);


    /**
     * What is the simplest UseCase?
     * - From the Ground Floor, go to 1st floor
     */
    @Test
    void from_groundFloor_1_person_takes_the_lift_and_gos_to_1st_floor() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        //GIVEN the following Lift Calls/Requests
        //GIVEN that the lift is on GroundFloor,
        // AND On ground Floor we have a person who wants to go to 1st floor
        floorCallsQueue.put(0, Arrays.asList(1));

        //WHEN, the Lift executes/responds to calls
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        //THEN

        //Lift itinerary
        assertEquals(Arrays.asList(0, 1), itinerary);
    }

    @Test
    void from_4h_Floor_2_persons_enter_and_press_buttons_for_1st_and_3rd_floor() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        //Floor Call Event, for 5th Floor
        floorCallsQueue.putIfAbsent(4, new ArrayList<>());

        //Once Lift arrived at 5th Floor, and people entered, they press the Buttons for the desired destination Floors
        floorCallsQueue.put(4, Arrays.asList(1, 3));

        //WHEN, the Lift executes/responds to calls
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 4, 3, 1), itinerary);
    }

    @Test
    void from_3rd_Floor_2_persons_enter_and_press_buttons_for_0_and_5th_floor_$no_direction$() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();


        //Once Lift arrived at 5th Floor, and people entered, they press the Buttons for the desired destination Floors
        floorCallsQueue.put(3, Arrays.asList(0, 5));

        //WHEN, the Lift executes/responds to calls
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        //THEN
        //Lift itinerary
        assertEquals(Arrays.asList(0, 3, 0, 5), itinerary);
    }

    @Test
    void from_groundFloor_2_persons_take_the_lift_and_they_press_5th_2nd_floor_buttons_$_up_direction_optimization() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        //GIVEN the following Lift Calls/Requests
        //GIVEN that the lift is on GroundFloor,
        // AND On ground Floor we have 2 persons pressing 5, 2
        floorCallsQueue.put(0, Arrays.asList(5, 2));

        //WHEN, the Lift executes/responds to calls


        //THEN
        final List<Integer> result = liftManager.processQueue(floorCallsQueue);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 2, 5), result);
    }

    @Test
    void on_2nd_Floor_1_request_for_3_and_3rd_floor_1_request_for_5() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();


        //Once Lift arrived at 2nd Floor, and people entered, they press the Buttons for the desired destination Floors
        floorCallsQueue.put(2, Arrays.asList(3));
        floorCallsQueue.put(3, Arrays.asList(5));

        //WHEN, the Lift executes/responds to calls
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        //THEN
        //Lift itinerary
        assertEquals(Arrays.asList(0, 2, 3, 5), itinerary);
    }

    @Test
    void from_0_to_5_From_5_to_3_From_3_to_4_From_4_to_0_$__UseCase_for_NoDirectionNeeded() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(5));
        floorCallsQueue.put(5, Arrays.asList(3));
        floorCallsQueue.put(3, Arrays.asList(4));
        floorCallsQueue.put(4, Arrays.asList(0));

        //WHEN, the Lift executes/responds to calls


        //THEN
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);
        System.out.println(itinerary);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 5, 3,4,0), itinerary);
    }

    @Test
    @Disabled
    void from_0_to_5_From_2_to_5() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(5));
        floorCallsQueue.put(2, Arrays.asList(5));

        //WHEN, the Lift executes/responds to calls


        //THEN
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);
        System.out.println(itinerary);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 2, 5), itinerary);
    }


    @Test
    @Disabled("Will work when I implement Direction")
    void from_5_to_0__from_1_to_3_from_4_to_2() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(5, Arrays.asList(0));
        floorCallsQueue.put(1, Arrays.asList(3));
        floorCallsQueue.put(4, Arrays.asList(2));

        //WHEN, the Lift executes/responds to calls


        //THEN
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);
        System.out.println(itinerary);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 5, 4, 2, 1, 3), itinerary);
    }

    @Test
    @Disabled
    void from_0_Floor_to_5th_From_2_to_4() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();


        floorCallsQueue.put(0, Arrays.asList(5));
        floorCallsQueue.put(2, Arrays.asList(4));

        //WHEN, the Lift executes/responds to calls


        //THEN
        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);
        System.out.println(itinerary);

        //Lift itinerary
        //assertEquals(Arrays.asList(0, 2, 5), result);
    }

    /**
     *
     */
    @Test
    @DisplayName("Go to 2nd floor, pickup the person and take it to the desired floor, e.g. 0")
    @Disabled
    void go_up_from_0_to_2() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        //GIVEN the following Lift Calls/Requests
        //On ground Floor we have a person who wants to go to 2nd floor
        floorCallsQueue.put(0, Arrays.asList(2));

        //WHEN, the Lift executes/responds to calls


        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 2), itinerary);
    }

    @Test
    @Disabled
    void go_up_from_2_to_5() {
        //Given a Building with 5 floors

        //Calls to the Lift are queued (in their,  order First In First Out)
        Map<Integer, int[]> liftCallsQueue = new LinkedHashMap<>();


        //- we have a call, at 2nd Floor, a queue of 3 People, all going to the 5th floors
        liftCallsQueue.put(2, new int[]{5, 5, 5});
    }
}
