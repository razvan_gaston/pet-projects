package raz.kata.lift;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class TargetFloorsDirectionTest {

    @Test
    void getDirection_down() {

        TargetFloorsDirection  direction = TargetFloorsDirection.getDirection(4, Arrays.asList(1,3));

        assertEquals(TargetFloorsDirection.DOWN, direction);

    }

    @Test
    void getDirection_up() {

        TargetFloorsDirection  direction = TargetFloorsDirection.getDirection(0, Arrays.asList(4,3));

        assertEquals(TargetFloorsDirection.UP, direction);

    }

    @Test
    void getDirection_up_and_down() {

        TargetFloorsDirection  direction = TargetFloorsDirection.getDirection(3, Arrays.asList(5,0));

        assertEquals(TargetFloorsDirection.NA, direction);

    }
}
