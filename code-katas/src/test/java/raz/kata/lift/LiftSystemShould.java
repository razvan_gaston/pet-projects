package raz.kata.lift;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import raz.kata.lift.v2.LiftSystem;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiftSystemShould {

    ILiftManager liftManager = null;

    @BeforeEach
    void setUp() {
        liftManager = new LiftSystem();
    }

    @Test
    void from_ground_floor_1_person_enters_and_press_button_for_1st_floor() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(1));

        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 1), itinerary);
    }

    /**
     * - ground_floor_1_person_enters_and_press_button_for_1st_floor
     * - then we are also called at the 2nd floor, where we have another person waiting go to the 5th floor
     */
    @Test
    void from_ground_floor_1_person_enters_and_press_button_for_1st_floor_2nd_to_5th() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(1));
        floorCallsQueue.put(2, Arrays.asList(5));

        final List<Integer> itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 1, 2, 5), itinerary);
    }
}
