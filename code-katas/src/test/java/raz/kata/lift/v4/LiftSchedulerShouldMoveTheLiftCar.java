package raz.kata.lift.v4;

import org.junit.jupiter.api.*;
import raz.kata.lift.ILiftManager;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("The Lift Scheduler Should Move the Lift Car from")
class LiftSchedulerShouldMoveTheLiftCar {

    ILiftManager liftManager = new LiftScheduler();

    Map<Integer, List<Integer>> floorCallsQueue;

    List<Integer> itinerary;

    @BeforeEach
    void setUp() {

        floorCallsQueue = new LinkedHashMap<>();

        itinerary = null;
    }

    @AfterEach
    void tearDown() {
        System.out.println("itinerary:" + itinerary);

        System.out.println("Floors Traveled:" + liftManager.getFloorsMovesCount());
    }

    @Test
    void from_0_to_1() {
        floorCallsQueue.put(0, Arrays.asList(1));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 1), itinerary);

        assertEquals(1, liftManager.getFloorsMovesCount());
    }

    /**
     * Pleaca de la origine de la 0,
     * - se duce la 4 si ia persoana
     * - si o coboara la 2
     * */
    @Test
    void from_restPosition0_from_4_to_2() {
        floorCallsQueue.put(4, Arrays.asList(2));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 4,2), itinerary);

    }

    @Test
    void from_restPosition0_from_1_to_0_From_4_to_2_3() {
        floorCallsQueue.put(1, Arrays.asList(0));
        floorCallsQueue.put(4, Arrays.asList(2,3));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0,4,3,2,1,0), itinerary);

    }

    @Test
    void from_1_to_5() {
        floorCallsQueue.put(1, Arrays.asList(5));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 1, 5), itinerary);

        //assertEquals(1, liftManager.getFloorsMovesCount(), "Floor traveled");
    }

    @Test
    void from_1_to_4_2() {
        floorCallsQueue.put(1, Arrays.asList(4, 2));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 1, 2, 4), itinerary);
    }

    @Test
    void from_2_to_1_1() {
        floorCallsQueue.put(2, Arrays.asList(1, 1));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 2, 1), itinerary);
    }

    @Test
    void from_0_to_5_2_5() {
        floorCallsQueue.put(0, Arrays.asList(5, 2, 5));

        itinerary = liftManager.processQueue(floorCallsQueue);

        assertEquals(Arrays.asList(0, 2, 5), itinerary);
    }

    @Test
    void from_0_to_5_From_5_to_3_From_3_to_4_From_4_to_0_$_Special_UseCase_Deplasarile_Sunt_Sudate_Una_In_captul_celeilalte_No_Optimization_needed() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(5));
        floorCallsQueue.put(5, Arrays.asList(3));
        floorCallsQueue.put(3, Arrays.asList(4));
        floorCallsQueue.put(4, Arrays.asList(0));

        //WHEN, the Lift executes/responds to calls


        //THEN
        itinerary = liftManager.processQueue(floorCallsQueue);


        //Lift itinerary
        assertEquals(Arrays.asList(0, 3, 4, 5, 4, 3, 0), itinerary);

        /*The hack/special case: processContiguousFloorRequests*/
        /*
itinerary:[0, 5, 3, 4, 0]
Floors Traveled:12
* */
//VS
//Algoritmul de optimizare EnRoute
/*
itinerary:[0, 3, 4, 5, 4, 3, 0]
Floors Traveled:10
* */
    }

    @Test
    void from_0_to_5_From_2_to_5_testUpAndUp() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(5));
        floorCallsQueue.put(2, Arrays.asList(5));

        itinerary = liftManager.processQueue(floorCallsQueue);


        assertEquals(Arrays.asList(0, 2, 5), itinerary);

        /*
        Fara Optimizare:
        itinerary:[0, 5, 2, 5]
        Floors Traveled:11
        * */

        /**
         * Cu Optimizarea EnRoute
         * itinerary:[0, 2, 5]
         * Floors Traveled:5
         * */
    }

    //https://www.codewars.com/kata/58905bfa1decb981da00009e/train/java
    @Test
    void from_1_to_3_from_2_to_4_from_4_to_5_testUpAndUp() {

        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(1, Arrays.asList(3));
        floorCallsQueue.put(2, Arrays.asList(4));
        floorCallsQueue.put(4, Arrays.asList(5));

        itinerary = liftManager.processQueue(floorCallsQueue);


        assertEquals(Arrays.asList(0, 1, 2, 3, 4, 5), itinerary);

        /*
         final int[] result = Dinglemouse.theLift(queues,5);
    assertArrayEquals(new int[]{0,1,2,3,4,5,0}, result);
        * */
    }

    @Test
    void from_1_to_0_from_4_to_2_from_5_to_3_testDownAndDown() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(1, Arrays.asList(0));
        floorCallsQueue.put(4, Arrays.asList(2));
        floorCallsQueue.put(5, Arrays.asList(3));

        itinerary = liftManager.processQueue(floorCallsQueue);
/*
    final int[] result = Dinglemouse.theLift(queues,5);
    assertArrayEquals(new int[]{0,5,4,3,2,1,0}, result);
* */

        assertEquals(Arrays.asList(0,5,4,3,2,1,0), itinerary);
    }

    /**
     * This useCase is against BusinessRules, but just curios what the Lift does
     */
    @Test
    void from_5_to_0_From_1_to_3_From_4_to_2_Down_Up_Down() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(5, Arrays.asList(0));
        floorCallsQueue.put(1, Arrays.asList(3));
        floorCallsQueue.put(4, Arrays.asList(2));

        itinerary = liftManager.processQueue(floorCallsQueue);
    }

    @Test
    void from_0_to_5_From_2_to_5() {
        Map<Integer, List<Integer>> floorCallsQueue = new LinkedHashMap<>();

        floorCallsQueue.put(0, Arrays.asList(5));
        floorCallsQueue.put(2, Arrays.asList(5));

        //WHEN, the Lift executes/responds to calls


        //THEN
        itinerary = liftManager.processQueue(floorCallsQueue);

        //Lift itinerary
        assertEquals(Arrays.asList(0, 2, 5), itinerary);
    }


}
