package raz.kata.lift.v4;

import org.junit.jupiter.api.Test;
import raz.kata.lift.v2.FloorEvent;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LiftCartTest {

    LiftCart liftCart = new LiftCart();

    @Test
    void compact_Optimize_Lift_Route_Going_Up() {
        FloorEvent firstRequest_0_to_5 = new FloorEvent(0, Arrays.asList(5));
        FloorEvent secondRequest_2_to_5 = new FloorEvent(2, Arrays.asList(5));

        FloorEvent mergedRequestsEnRoute = liftCart.mergeFloorRequestGroup(firstRequest_0_to_5, Arrays.asList(secondRequest_2_to_5));


        List<Integer> boardingFloors = mergedRequestsEnRoute.getBoardingFloors().collect(Collectors.toList());

        assertEquals(Arrays.asList(0, 2), boardingFloors);

        List<Integer> destinations = mergedRequestsEnRoute.getDestinationFloors().collect(Collectors.toList());

        assertEquals(Arrays.asList(5), destinations);

    }

    @Test
    void compact_Optimize_Lift_Route_Down_and_Down() {
        FloorEvent firstRequest = new FloorEvent(1, Arrays.asList(0));
        FloorEvent secondRequest = new FloorEvent(4, Arrays.asList(2));
        FloorEvent thirdRequest = new FloorEvent(5, Arrays.asList(3));

        FloorEvent mergedRequestsEnRoute = liftCart.mergeFloorRequestGroup(firstRequest, Arrays.asList(secondRequest, thirdRequest));


        List<Integer> boardingFloors = mergedRequestsEnRoute.getBoardingFloors().collect(Collectors.toList());

        assertEquals(Arrays.asList(5, 4, 1), boardingFloors, "boardingFloors");

        List<Integer> destinations = mergedRequestsEnRoute.getDestinationFloors().collect(Collectors.toList());

        assertEquals(Arrays.asList(3, 2, 0), destinations, "destinations");

    }
}
