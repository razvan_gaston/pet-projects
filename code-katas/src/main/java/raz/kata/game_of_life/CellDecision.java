package raz.kata.game_of_life;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Intuitiv am aplicat Single Resp Principle,
 * De exemplu daca maine cineva vine sa schimbe regurile dupa care o celula traieste sau moare,
 * fac schimbarea doar aici in clasa asta.
 * <p>
 * Deci am izolat Behavioral-ul asta specific intr-o singura clasa.
 * <p>
 * So this class has only one Reason to Change. that is, if the business Decides to change the Rules when a cell live/dies
 */
public class CellDecision {

    /* Celul in cauza care este verificata */
    final Cell theCell;

    public CellDecision(Cell theCell) {
        this.theCell = theCell;
    }

    /**
     * Based on the current Cell Context/SnapShot
     * and considering the game rules,
     * care ar trebui sa fie starea celulei?
     *
     * @return Dead or Alive?
     */
    public CellState outcome() {
        Collection<Cell> neighbours = theCell.neighbours();

        Collection<Cell> liveNeighbours = neighbours
                .stream()
                .filter(cell -> cell.getState() == CellState.ALIVE)
                .collect(Collectors.toList());

        Collection<Cell> deadNeighbours = neighbours
                .stream()
                .filter(cell -> cell.getState() == CellState.DEAD)
                .collect(Collectors.toList());

        if (theCell.isAlive() && liveNeighbours.size() < 2) {
            //1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
            return CellState.DEAD;
        }

        if (theCell.isAlive() && liveNeighbours.size() > 3) {
            //2. Any live cell with more than three live neighbours dies, as if by overcrowding.
            return CellState.DEAD;
        }

        if (theCell.isAlive() &&
                (liveNeighbours.size() == 2 || liveNeighbours.size() == 3)) {
            //3. Any live cell with two or three live neighbours lives on to the next generation.
            return CellState.ALIVE;
        }

        if (theCell.isDead() && liveNeighbours.size() == 3) {
            //4. Any dead cell with exactly three live neighbours becomes a live cell.
            return CellState.ALIVE;
        }


        //Here reached here with execution, and no rule applied to this current cell context, so do nothing
        //This state/scenario is unspecified/unHandled by the game rules, so do nothing

//        System.out.println("WARN/INFO, cell:" + this.theCell.toStringDetailed() + " liveNeighbours:" + liveNeighbours.size() + " deadNeighbours:" + deadNeighbours.size()
//                + " \n this Scenario is Unspecified in the Game rules, returning the same cell state");

        return theCell.getState();
    }

}
