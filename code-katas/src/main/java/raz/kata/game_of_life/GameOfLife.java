package raz.kata.game_of_life;

import raz.kata.game_of_life.event.Observer;
import raz.kata.game_of_life.event.StateChangeEvent;
import raz.kata.game_of_life.event.StateUpdater;
import raz.kata.util.MatrixUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static raz.kata.game_of_life.event.StateChangeReason.INITIAL_EVALUATION;

public class GameOfLife {

    final List<Observer> eventListeners = new ArrayList<>();

    //This Object generates the Next Generation of Conway's game of life
    final StateUpdater stateUpdater = new StateUpdater(this);

    void attach(Observer observer) {
        eventListeners.add(observer);
    }

    public void notifyAllObservers(StateChangeEvent stateChangeEvent) {
        eventListeners.forEach(observer -> observer.onEvent(stateChangeEvent));
    }

//    private final int rowsNum;
//    private final int colsNum;

    //initGeneration
    //generationZero
    //origin Generation
    private Cell[][] gameMatrix = null;


    public GameOfLife(Cell[][] matrix) {
        Objects.nonNull(matrix);

        //this.gameMatrix = matrix;
        this.gameMatrix = new Cell[matrix.length][matrix.length];

        attach(stateUpdater);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                Cell incoming = matrix[i][j];
                gameMatrix[i][j] = new Cell(incoming.getState(), i, j, gameMatrix, this);
            }
        }
    }

    public GameOfLife(int rowsNum, int colsNum) {
//        this.rowsNum = rowsNum;
//        this.colsNum = colsNum;

        gameMatrix = new Cell[rowsNum][colsNum];

        attach(stateUpdater);

        MatrixUtil.initMatrixWithValueSupplier(gameMatrix,
//                () -> new Cell(CellState.O),
//                (cell)->{cell.setColIdx();}
                (rowColPair) ->
                        new Cell(CellState.DEAD, rowColPair.rowIdx(), rowColPair.colIdx(), gameMatrix, this)
                //(xASZ1) this Code is not invoked/executed here/at this point, this will be lazy evaluated/it will be executed later


        );
    }

    public Cell[][] getGameMatrix() {
        return gameMatrix;
    }

    /**
     * Se initializeaza GameOfLife cu generatia/SnapShot-ul de la care porneste Universe Simulation.
     * este Starea Initiala, StartGeneration, InitGeneration, Origin, starting position
     */
    public void setLiveCells(Collection<RowColPair> liveCells) {
        for (RowColPair cellCoords : liveCells) {
            gameMatrix[cellCoords.rowIdx()][cellCoords.colIdx()].live();
            //gameMatrix[0][0].live();
        }
    }

    /**
     * Plecand de la Origin/Starting Position, we start the Universe Simulation,
     * se verifica fiecare celula si aflam daca este nevoie
     * sa schimbam starea vreunei celule, care la randul ei might trigger a chain reaction,
     * and we keep going until we reach equilibrium, that is, no more transitions are required and thes simulation is over,
     * it can not transition to any new states
     */
    public void playTheGame() {
        System.out.println("GAME Started");
        MatrixUtil.showMatrix(gameMatrix);

        Cell cell;
        for (int rowIdx = 0; rowIdx < gameMatrix.length; rowIdx++) {
            Cell[] oneLine = gameMatrix[rowIdx];
            for (int colIdx = 0; colIdx < oneLine.length; colIdx++) {
                cell = oneLine[colIdx];

//                if(cell.isStateChangeIsRequired()!=null){
//                    //state change is required
//                    //we enqueue/trigger  a CellState Change Event
//
//                    StateChangeEvent stateChangeRequest = new StateChangeEvent(cell, cell.isStateChangeIsRequired(), StateChangeReason.INITIAL_EVALUATION);
//                }
                cell.checkIfStateChangeIsRequired(INITIAL_EVALUATION);
            }
        }
    }

}
