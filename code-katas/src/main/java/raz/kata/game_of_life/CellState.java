package raz.kata.game_of_life;

public enum CellState {

    DEAD("o"), ALIVE("x");

    private final String strVal;

    CellState(String strRepresentation) {
        this.strVal = strRepresentation;
    }

    @Override
    public String toString() {
        return strVal;
    }

    public static CellState fromValue(String val){
        for(CellState enumVal : CellState.values()){
            if(enumVal.strVal.equals(val) ){
                return enumVal;
            }
        }
        throw new IllegalArgumentException("Invalid CellState value:"+val);
    }
}
