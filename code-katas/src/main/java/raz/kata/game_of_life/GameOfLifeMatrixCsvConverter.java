package raz.kata.game_of_life;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GameOfLifeMatrixCsvConverter {


    public Cell[][] loadFromCsv(Path csvFilePath) throws IOException {
        Cell[][] gameMatrix;

        List<String> csvRows = readCsvLines(csvFilePath);
        gameMatrix = new Cell[csvRows.size()][];

        int rowIdx = 0;
        for (String oneRow : csvRows) {
            String[] oneRowValues = oneRow.split(Pattern.quote("|"));

            List<String> rowValues = Stream.of(oneRowValues)
                    .map(s -> s.trim())
                    .collect(Collectors.toList());

            //System.out.println(rowValues);

            Cell[] oneMatrixRow = oneMatrixRow(rowValues, rowIdx, gameMatrix);

            gameMatrix[rowIdx] = oneMatrixRow;

            rowIdx++;
        }


        return gameMatrix;
    }

    private List<String> readCsvLines(Path csvFilePath) throws IOException {
        try (Stream<String> csvLine = Files.lines(csvFilePath)) {
            List<String> matrixRows = csvLine.filter(strLine -> !strLine.isEmpty())
                    .collect(Collectors.toList());
            return matrixRows;
        }
    }

    private Cell[] oneMatrixRow(List<String> rowValues, int rowIdx, Cell[][] gameMatrix) {
        Cell[] oneMatrixRow = new Cell[rowValues.size()];
        Cell cell = null;


        for (int idx = 0; idx <= rowValues.size() - 1; idx++) {
            cell = new Cell(
                    CellState.fromValue(rowValues.get(idx)),
                    rowIdx,
                    idx,
                    gameMatrix
            );
            oneMatrixRow[idx] = cell;
        }

        return oneMatrixRow;
    }

}
