package raz.kata.game_of_life;

public class RowColPair {

    private final int rowIdx;
    private final int colIdx;

    public RowColPair(int rowIdx, int colIdx) {
        this.rowIdx = rowIdx;
        this.colIdx = colIdx;
    }

    public int rowIdx() {
        return rowIdx;
    }

    public int colIdx() {
        return colIdx;
    }
}
