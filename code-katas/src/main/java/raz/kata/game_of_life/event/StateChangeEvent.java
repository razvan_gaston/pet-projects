package raz.kata.game_of_life.event;

import raz.kata.game_of_life.Cell;
import raz.kata.game_of_life.CellState;

public class StateChangeEvent {

    final Cell cell;
    final CellState newState;
    final StateChangeReason reason;

    public StateChangeEvent(Cell cell, CellState newState, StateChangeReason reason) {
        this.cell = cell;
        this.newState = newState;
        this.reason = reason;
    }
}
