package raz.kata.game_of_life.event;

public enum StateChangeReason {

    NEIGHBOUR_UPDATE,
    INITIAL_EVALUATION
}
