package raz.kata.game_of_life.event;

import raz.kata.game_of_life.Cell;
import raz.kata.game_of_life.CellState;
import raz.kata.game_of_life.GameOfLife;
import raz.kata.util.MatrixUtil;

import java.util.Collection;

//(this is the Event Listener, the one who consumes/handles the events)
public class StateUpdater implements Observer {

    final GameOfLife gameOfLife;

    public StateUpdater(GameOfLife gameOfLife) {
        this.gameOfLife = gameOfLife;
    }

    private int genCount;

    @Override
    public void onEvent(StateChangeEvent stateChangeRequest) {
        processStateChangeRequest(stateChangeRequest);
    }

    private void processStateChangeRequest(StateChangeEvent stateChangeRequest) {
        genCount++;

        Cell toBeUpdated = stateChangeRequest.cell;

        CellState dueState = stateChangeRequest.newState;

        //System.out.println("CURRENT GoF STATE");
        //MatrixUtil.showMatrix(gameOfLife.getGameMatrix());

        if (dueState == CellState.ALIVE)
            toBeUpdated.live();
        else
            toBeUpdated.die();

        try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Next Generation:"+(genCount));
        MatrixUtil.showMatrix(gameOfLife.getGameMatrix());

        Collection<Cell> neighbours = toBeUpdated.neighbours();

        for (Cell oneNeighbour : neighbours) {
            oneNeighbour.checkIfStateChangeIsRequired(StateChangeReason.NEIGHBOUR_UPDATE);
        }
    }
}
