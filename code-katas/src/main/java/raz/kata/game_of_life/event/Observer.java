package raz.kata.game_of_life.event;

public interface Observer {

    void onEvent(StateChangeEvent stateChangeRequest);
}
