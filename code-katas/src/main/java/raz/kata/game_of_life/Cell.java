package raz.kata.game_of_life;

import raz.kata.game_of_life.cell.Neighbours;
import raz.kata.game_of_life.event.StateChangeEvent;
import raz.kata.game_of_life.event.StateChangeReason;

import java.util.Collection;
import java.util.Objects;

import static raz.kata.game_of_life.CellState.ALIVE;
import static raz.kata.game_of_life.CellState.DEAD;

public class Cell {

    private CellState state = CellState.DEAD;

    private final int rowIdx;
    private final int colIdx;
    private Cell[][] gameMatrix;
    private final GameOfLife gameOfLife;

    public Cell(CellState state, int rowIdx, int colIdx, Cell[][] gameMatrix) {
        this.state = state;
        this.rowIdx = rowIdx;
        this.colIdx = colIdx;
        this.gameMatrix = gameMatrix;
        gameOfLife = null;
    }

    public Cell(CellState state, int rowIdx, int colIdx, Cell[][] gameMatrix, GameOfLife gameOfLife) {
        this.state = state;
        this.rowIdx = rowIdx;
        this.colIdx = colIdx;
        this.gameMatrix = gameMatrix;
        //this(state, rowIdx, colIdx, gameMatrix);
        this.gameOfLife = gameOfLife;
    }

    public void live() {
        this.state = ALIVE;
    }
    public void die() {
        this.state = DEAD;
    }

    @Override
    public String toString() {
        return
                // "{" + this.rowIdx + "," + this.colIdx + "} " +
                state.toString();
    }

    public String toStringDetailed(){
        return
                 "[" + this.rowIdx + "," + this.colIdx + "] " +
                state.toString();
    }

    @Override
    public boolean equals(Object otherCell) {
        if (this == otherCell) return true;
        if (otherCell == null || getClass() != otherCell.getClass()) return false;
        Cell cell = (Cell) otherCell;
        return rowIdx == cell.rowIdx &&
                colIdx == cell.colIdx &&
                state == cell.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, rowIdx, colIdx);
    }

    public Collection<Cell> neighbours() {
        return Neighbours.cellNeighbours(this);
    }


    public int getRowIdx() {
        return rowIdx;
    }

    public int getColIdx() {
        return colIdx;
    }

    public Cell[][] getGameMatrix() {
        return gameMatrix;
    }

    public CellState getState() {
        return state;
    }

    public boolean isDead() {
        return this.state == CellState.DEAD;
    }

    public boolean isAlive() {
        return this.state == ALIVE;
    }

    public void checkIfStateChangeIsRequired(StateChangeReason reason) {
        CellDecision cellDecision = new CellDecision(this);
        CellState dueState = cellDecision.outcome();
        if (state != dueState) {
            //state change is required
            //return dueState;

            //we enqueue/trigger/we fire a CellState Change Event
            StateChangeEvent stateChangeRequest = new StateChangeEvent(this,dueState , reason);

            this.gameOfLife.notifyAllObservers(stateChangeRequest);
        }
    }
}

