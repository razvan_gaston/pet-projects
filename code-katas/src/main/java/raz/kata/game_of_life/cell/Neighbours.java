package raz.kata.game_of_life.cell;

import raz.kata.game_of_life.Cell;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Iterate all Possible Neighbours,
 * si cei care nu arunca IndexOutOfBonds, return
 * <p>
 * and Return those which are within Bounds of the Matrix
 */
public enum Neighbours {

    N(-1, 0),//UP
    NW(-1, -1),
    NE(-1, +1),
    S(+1, 0),//DOWN
    SW(+1, -1),
    SE(+1, +1),
    E(0, +1),//RIGHT
    W(0, -1),//LEFT
    ;//LEFT

    final int rowIdxOffset;

    //Deplasament Coloana
    final int colIdxOffset;


    Neighbours(int rowIdxOffset, int colIdxOffset) {
        this.rowIdxOffset = rowIdxOffset;
        this.colIdxOffset = colIdxOffset;
    }

    /**
     * - iterate all possible neighbours,
     * return those which haven't thrown indexOutOfBounds
     */
    public static Collection<Cell> cellNeighbours(/*Cell[][] matrix,*/ Cell cell) {
        Set<Cell> neighbours = new HashSet<>();
        Cell potentialNeighbour;
        for (Neighbours position : Neighbours.values()) {
            int rowIdx = position.rowIdxOffset == 0 ? cell.getRowIdx() : cell.getRowIdx() + position.rowIdxOffset;
            int colIdx = position.colIdxOffset == 0 ? cell.getColIdx() : cell.getColIdx() + position.colIdxOffset;

            try {
                potentialNeighbour = cell.getGameMatrix()[rowIdx][colIdx];
                //System.out.println(position+" Vecin ["+rowIdx+"]["+colIdx+"]");
                neighbours.add(potentialNeighbour);
            } catch (ArrayIndexOutOfBoundsException e) {
                //System.out.println("Nu exista vecin la:" + position);
            }
        }
        return neighbours;
    }
}
