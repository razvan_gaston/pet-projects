package raz.kata.lift.v4;

import raz.kata.lift.Direction;
import raz.kata.lift.v2.FloorEvent;
import raz.kata.lift.v2.RoutableLiftRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class LiftCart {

    private final List<Integer> itinerary = new ArrayList<>();

    private int currentFloor = 0;

    private AtomicInteger floorsTraveled = new AtomicInteger(0);

    public LiftCart() {
        itinerary.add(currentFloor);
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Events are processed in the Order they arrived, Floor By Floor
     */
    public void processFloorRequest(FloorEvent requestedAtFloor) {
        serveFloorRequest(requestedAtFloor);
    }

    private void serveFloorRequest(RoutableLiftRequest requestedAtFloor) {
        List<Integer> cartItineraryForRequest = new ArrayList<>();

        //1 ST we need to add the pickupFloors
        List<Integer> boardingFloors = requestedAtFloor.getBoardingFloors().collect(Collectors.toList());
        cartItineraryForRequest.addAll(boardingFloors);

        //unPack/FlatMap the destinations (which are already sorted based on lift direction)
        //Now add the destinations
        cartItineraryForRequest.addAll(requestedAtFloor.getDestinationFloors().collect(Collectors.toList()));

        /**
         * Mi-e teama ca daca sortez itinerary ca nu cumva in anumit scenariu (dar nu am gasit unu clar)
         * */
        if (requestedAtFloor.sameDirectionMergedRequest()) {
            //Aici sunt sigur ca nu am Up and Down, ci doar in aceeasi directie,
            //caz in care I am safe sa sortez conform directiei
            FloorEvent.sortTargetDestinations(cartItineraryForRequest, requestedAtFloor.getRequestDirection());
        }

        //move the cart according to itinerary to fulfill this FloorRequest
        cartItineraryForRequest.stream()
                .forEach(this::goToFloor);
    }

    /**
     * @param alongTheWay - boarding Floors EnRoute while serving firstRequest
     */
    public FloorEvent mergeFloorRequestGroup(FloorEvent firstRequest, List<FloorEvent> alongTheWay) {

        /*- si lista List<Integer> targetDestinations, o ai deja,
        Deci aici poti sa pui la gramada destinatiile comasate, ordinea nu conteaza ca asta oricum va fi sortata conform UP/DOWN
        */
        List<Integer> compactedDest = new ArrayList<>(firstRequest.getDestinationFloors().collect(Collectors.toList()));
        for (FloorEvent enRouteBoardingFloor : alongTheWay) {
            for (Integer dest : enRouteBoardingFloor.getDestinationFloors().collect(Collectors.toList())) {
                if (!compactedDest.contains(dest)) {
                    compactedDest.add(dest);
                }
            }
        }


        List<Integer> boardingFloors = new ArrayList<>(Arrays.asList(firstRequest.getFloorNumber()));

        for (FloorEvent boardingFloorEnRoute : alongTheWay) {
            //BoardingFloors can not be duplicated, so no need to check if I already have it
            boardingFloors.add(boardingFloorEnRoute.getFloorNumber());
        }

      /*
      - tu stii directia, daca e down de ce nu prioritizezi on-boardingul cel mai costisitor, in cazul asta,
        ala de la 5, si la sfarsit il pui pe cel mai putin costisitor
        */
        if (firstRequest.getRequestDirection() == Direction.UP) {
            boardingFloors.sort(Integer::compareTo);
        } else {
            //Daca e Down, cel mai costisitor este etajul cel mai mare, asa ca du-te si ia-l pe ala primul
            boardingFloors.sort(
                    Comparator.comparingInt(Integer::intValue).reversed()
            );
        }

        FloorEvent floorRequestsEnRouteMergedOptimized = new FloorEvent(firstRequest.getRequestDirection(), boardingFloors, compactedDest);
        return floorRequestsEnRouteMergedOptimized;
    }


    private void goToFloor(int nextFloor) {
        if (nextFloor == currentFloor)
            return;

        itinerary.add(nextFloor);

        //you have to move the cart floor by floor, either by increment or decrement
        //this.currentFloor = nextFloor; - here you teleport, you can just set the current floor from 1 to 5
        //you have to go there floor by floor, sequential

        advanceFloorByFloor(nextFloor);
    }

    private enum DIR {UP, DOWN}

    private void advanceFloorByFloor(int target) {
        if (target > currentFloor)
            move(target, DIR.UP);
        else
            move(target, DIR.DOWN);

    }

    private void move(int target, DIR dir) {
        while (currentFloor != target) {
            if (dir == DIR.UP) {
                currentFloor++;
            } else {
                currentFloor--;
            }
            //measure the floors traveled, so I know which is the optimal route
            floorsTraveled.incrementAndGet();
        }
    }

    public int currentFloor() {
        return currentFloor;
    }

    public int floorsTraveled() {
        return floorsTraveled.get();
    }

    public List<Integer> itinerary() {
        return itinerary;
    }
}
