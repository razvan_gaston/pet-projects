package raz.kata.lift.v4;

import raz.kata.lift.Direction;
import raz.kata.lift.ILiftManager;
import raz.kata.lift.v2.FloorEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LiftScheduler implements ILiftManager {

    private LiftCart liftCart = new LiftCart();


    /**
     * Floor Events, in the order as they arrived
     */
    List<FloorEvent> floorEventsBuffer = new ArrayList<>();

    List<FloorEvent> primaryBuffer;
    List<FloorEvent> secondaryBuffer;

    @Override
    public int getFloorsMovesCount() {
        return liftCart.floorsTraveled();
    }

    /**
     * Business Rules:
     * - The Lift only goes up or down!
     * <p>
     * - The Lift never changes direction until there are no more people wanting to get on/off in the direction it is already travelling
     * <p>
     * "Only people going the same direction as the Lift may enter it"
     * So I expect that we captured/Buffered/collected only floorRequests in the same direction
     * "The Lift never changes direction until there are no more people wanting to get on/off in the direction it is already travelling"
     * - if people want to go the opposite direction they will wait until the lift is free, and then enqueue their requests
     * for the other direction
     *
     * @param floorCallsQueue - We don't have a Live Event System which keeps adding events to be processed,
     *                        - so we work with a SNAPSHOTS/a BUFFER of events
     * @return A list of all the floors that the Lift stopped at (in the order visited!)
     */
    @Override
    public List<Integer> processQueue(Map<Integer, List<Integer>> floorCallsQueue) {
        floorEventsBuffer.clear();
//        if()primaryBuffer.clear();
//        secondaryBuffer.clear();

        //Enqueue Phase
        for (Map.Entry<Integer, List<Integer>> pickupFloor_DropOffFloors : floorCallsQueue.entrySet()) {
            floorEventsBuffer.add(new FloorEvent(pickupFloor_DropOffFloors.getKey(), pickupFloor_DropOffFloors.getValue()));
        }

        //processContiguousFloorRequests();

        processBuffer();

        return liftCart.itinerary();
    }

    private void processBuffer() {
        //The first floorRequest Determines the Direction
        //Direction direction = Direction.getDirection(liftCart.getCurrentFloor(), floorEventsBuffer.get(0).getFloorNumber());
        Direction direction = floorEventsBuffer.get(0).getRequestDirection();

        primaryBuffer = floorEventsBuffer.stream()
                .filter(floorRequest -> floorRequest.getRequestDirection() == direction)
                .collect(Collectors.toList());

        secondaryBuffer = floorEventsBuffer.stream()
                .filter(floorRequest -> floorRequest.getRequestDirection() != direction)
                .collect(Collectors.toList());

        if (!primaryBuffer.isEmpty()) {
            processDirectionBuffer(primaryBuffer);
        }

        if (!secondaryBuffer.isEmpty()) {
            processDirectionBuffer(secondaryBuffer);
        }

    }

    /**
     * However, the 1st event determines the Direction,
     * so while going in that direction AND Serving the floors from the 1st request
     * we stop at the floors in our direction AND In our target floors range
     */
    private void processDirectionBuffer(List<FloorEvent> sameDirection) {
        if (sameDirection.size() == 1) {
            liftCart.processFloorRequest(sameDirection.get(0));
        } else {

            //int lastElemIndex = sameDirection.size() - 1;
            FloorEvent mainFloorRequest = sameDirection.get(0);

            List<FloorEvent> alongTheWayWithinRange = sameDirection.subList(1, sameDirection.size());


            FloorEvent mergedRequestsEnRoute = liftCart.mergeFloorRequestGroup(mainFloorRequest, alongTheWayWithinRange);

            liftCart.processFloorRequest(mergedRequestsEnRoute);
        }
    }

    /**
     * TODO: Am studiat mai bine, si even though seemed like a Brilliant Epiphany,
     * on further studying and running some tests, it is quite a hack/Special UseCase, so I will remove this,
     * see UnitTest - from_0_to_5_From_5_to_3_From_3_to_4_From_4_to_0_$_Special_UseCase_Deplasarile_Sunt_Sudate_Una_In_captul_celeilalte_No_Optimization_needed
     *
     * process first N floorRequests as long a the lift Never travels empty
     * - so the destination of a floor request is the start of a new request
     */
//    private void processContiguousFloorRequests() {
//        Iterator<FloorEvent> iterator = floorEventsBuffer.iterator();
//        FloorEvent floorEvent;
//        while (iterator.hasNext()) {
//            floorEvent = iterator.next();
//
//            if (floorEvent.getFloorNumber() != liftCart.getCurrentFloor())
//                return;
//
//            liftCart.processFloorRequest(floorEvent);
//            iterator.remove();
//        }
//    }
}
