package raz.kata.lift;

import raz.kata.lift.v2.FloorEvent;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * When called, the Lift will stop at a floor even if it is full, although unless somebody gets off nobody else can get on!
 * <p>
 * - When empty the Lift tries to be smart...
 */

//Lift Manager, receives the Lift Calls, and operates/moves/commands the LiftCart
public class LiftManager implements ILiftManager {


    final int lastFloor;

    //Map<Integer, List<Integer>> pickupFloor_DropOffFloors = null;
    List<FloorEvent> floorEvents = null;

    private LiftCar liftCar;

    public LiftManager(int lastFloor) {
        this.lastFloor = lastFloor;
    }

    /**
     * A list of all floors that the Lift stopped at (in the order visited!)
     */
    private List<Integer> itinerary() {
        return liftCar.itinerary();
    }

    /**
     * Ordinea in care a fost chemat liftul la diferite etaje,
     * - the keys, reprezinta etajele (in ordinea in) care au chemat liftul
     * (ordinea in care oamenii aflati la diferite etaje au apasat pe butonul de chemat liftul)
     * <p>
     * - The Values, reprezinta comenzile/cerintele de destinatie ale persoanelor
     * per fiecare etaj, ce butoane s-au apasat pe Consola, adica ce etaje/destiatii au fost alese
     *
     * @param floorCallsQueue - We don't have a Live Event System which keeps adding events to be processed,
     *                        - so we work with a SNAPSHOTS/a BUFFER of events
     */
    @Override
    public List<Integer> processQueue(Map<Integer, List<Integer>> floorCallsQueue) {

        floorEvents = new LinkedList<>();
        liftCar = new LiftCar();

        //Enqueue Phase
        for (Map.Entry<Integer, List<Integer>> pickupFloor_DropOffFloors : floorCallsQueue.entrySet()) {
            floorEvents.add(new FloorEvent(pickupFloor_DropOffFloors.getKey(), pickupFloor_DropOffFloors.getValue()));
        }

        //Process Phase
        processFloorRequestsNoDirectionChange();

        //What ever events have left have a direction change
        processFloorRequests();

        return this.itinerary();
    }

    /**
     * Events are processed in the Order they arrived
     * <p>
     * However, the 1st event determines the Direction
     * so while going in that directions,
     * we stop at the floors in our direction
     * the other events the have the same directions are pickedUp and processed
     * <p>
     * XXX: But what if a floor that we stop EN-Route, changes direction?
     * that is not allowed,
     * The Business Rules, state: Only people going the same direction as the Lift may enter it
     */
    private void processFloorRequests() {

        //The first floorRequest Determines the Direction
        Direction direction = Direction.getDirection(liftCar.getCurrentFloor(), floorEvents.get(0).getFloorNumber());

        //process each floorRequest in their order
        for (FloorEvent floorEvent : floorEvents) {
            //Move the elevator car to the requested floor
            liftCar.pickupFrom(floorEvent.getFloorNumber());


            //Once the ppl pressed the buttons,
            //we need to take the ppl to the desired Destination Floors
            liftCar.dropOffTo(floorEvent.getDestinationFloors().collect(Collectors.toList()));
        }

        //TODO , I must make sure that when Grouping By - the order from stream is maintained
        Map<Direction, List<FloorEvent>> floorRequestsByDirection = floorEvents.stream()
                .collect(
                        Collectors.groupingBy(floorEvent -> floorEvent.getRequestDirection())
                );
        //all I have to do is to Merge/FlatMap the list of of floors see XXad1
    }

    //process first N Events as long as I don't have direction change
    private void processFloorRequestsNoDirectionChange() {
        Iterator<FloorEvent> iterator = floorEvents.iterator();
        while (iterator.hasNext()) {
            FloorEvent floorEvent = iterator.next();

            if (floorEvent.getFloorNumber() == liftCar.getCurrentFloor()) {
                //Once the ppl pressed the buttons,
                //we need to take the ppl to the desired Destination Floors
                liftCar.dropOffTo(floorEvent.getDestinationFloors().collect(Collectors.toList()));

                iterator.remove();
            } else {
                return;
            }
        }
    }

    private void resetItinerary() {
//        itinerary.clear();
//        itinerary.add(restPosition);
    }

}
