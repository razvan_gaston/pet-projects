package raz.kata.lift;

import java.util.List;
import java.util.stream.Collectors;

/**
 * From the pickUp Floor,
 * we must determine if the requested dropOff Floors are in the same direction,
 * <p>
 * If they are not in the same direction
 */
public enum TargetFloorsDirection {

    UP,
    DOWN,

    //NOT APPLICABLE
    NA;

    public static TargetFloorsDirection getDirection(int currentFloor, List<Integer> targetFloors) {
        TargetFloorsDirection result;

        //sor asc the target floor
        //targetFloors.sort(Integer::compareTo);

        int minFloor = targetFloors.stream().collect(Collectors.minBy(Integer::compareTo)).get();
        int maxFloor = targetFloors.stream().collect(Collectors.maxBy(Integer::compareTo)).get();

        if (currentFloor > maxFloor) {
            //target floors are below, so elevator is going down
            return DOWN;
        } else if (currentFloor < minFloor) {
            //target floors are above,
            return UP;
        } else {
            return NA;
        }

    }
}
