package raz.kata.lift;

public enum Direction {

    UP, DOWN;

  public  static Direction getDirection(int currentFloor, int nextFloor) {
        if (currentFloor == nextFloor)
            throw new IllegalArgumentException("Why do you need direction, you don'y need to move the Lift Car");

        return nextFloor > currentFloor ? UP : DOWN;
    }
}
