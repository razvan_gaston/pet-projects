package raz.kata.lift.v3;

import raz.kata.lift.TargetFloorsDirection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LiftCar {

    private int currentFloor;
    private final List<Integer> itinerary = new ArrayList<>();


    /**
     * XXad1
     * Ce inseamna de fapt processFloorRequest?
     * it produces a List<Integer> itinerary
     * care descrie etajele unde trebuie sa mearga cabina liftului
     * <p>
     * adica produce o lista de etaje a fi parcursa de cabina
     * by :
     * 1/ - sorting the destination floors
     * 2/ prepanding the fromFloor
     */
    public void processFloorRequest(int floorNumber, FloorEventLazy floorRequest) {

        //Move the elevator car to the requested floor
        if (currentFloor != floorNumber) {
            goToFloor(floorNumber);
        }

        //Once arrived at the requested Floor
        //As ppl press the buttons they want to go, accept their request/designated destination floors
        //and start serving/process them

        //this is LAZY LOADING, we only READ THE TARGET FLOORS when we arrive at that floor/process that floorRequest
        List<Integer> destinations = floorRequest.getDestinationFloorsFunc().get();
        //Once the LiftCar knows where the ppl in want to go,
        // it has al the data, it is independent,
        //it can decide on its own the itinerary

        goToTargetFloors(destinations);
    }

    private void goToTargetFloors(List<Integer> destinationsFloors) {
        TargetFloorsDirection direction = TargetFloorsDirection.getDirection(currentFloor, destinationsFloors);
        switch (direction) {
            case UP:
                //all the target floors are ABOVE/UP the current floor so we visit them in ASC order
                destinationsFloors.sort(Integer::compareTo);
                break;
            case DOWN:
                //all the target floors are BELOW/DOWN the current floor so visit them in DESC order
                destinationsFloors.sort(Comparator.comparingInt(Integer::intValue).reversed());
                break;
            default:
                break;
        }

        destinationsFloors.stream()
                .forEach(this::goToFloor);
    }

    private void goToFloor(int nextFloor) {
        itinerary.add(nextFloor);
        this.currentFloor = nextFloor;
    }

}
