package raz.kata.lift.v3;

import java.util.List;
import java.util.function.Supplier;

public class FloorEventLazy  {

    private final int floorNumber;

    private final Supplier<List<Integer>> destinationFloorsFunc;


    public FloorEventLazy(int floorNumber, Supplier<List<Integer>> destinationFloorsSupplier) {
        this.floorNumber = floorNumber;
        this.destinationFloorsFunc = destinationFloorsSupplier;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public Supplier<List<Integer>> getDestinationFloorsFunc() {
        return destinationFloorsFunc;
    }
}
