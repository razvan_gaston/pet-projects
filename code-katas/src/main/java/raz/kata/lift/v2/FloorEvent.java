package raz.kata.lift.v2;

import raz.kata.lift.Direction;
import raz.kata.lift.TargetFloorsDirection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * The Lift Stopped at a Floor
 * - either the Lift was called at that floor
 * - or it stopped at the Floor Because it was a Destination Floor
 * <p>
 * - when a Lift Stops at a floor, people can enter and press buttons for Destination floors
 */
public class FloorEvent implements RoutableLiftRequest {


    private final Integer floorNumber;
    private final List<Integer> boardingFloors;

    private final List<Integer> destinationFloors = new ArrayList<>();
    private final Direction requestDirection;

//    public FloorEvent(int floorNumber) {
//        this.floorNumber = floorNumber;
//    }

    public FloorEvent(int floorNumber, List<Integer> targetDestinations) {
        this.floorNumber = floorNumber;
        //you either process"Regular" FloorRequest
        //or you have a FloorRequestGroup, where you combined multiple requests
        //so these are mutual exclusive
        boardingFloors = null;

        //After the targetDestinations have been sorted
        // => I know which floor is next, so I can determine the direction
        sortTargetDestinations(targetDestinations);
        destinationFloors.addAll(targetDestinations);

        //to determine Direction of this Floor Request
        /**
         * - WHERE do you want to go?
         * - Going Up or Going Down?
         * */
        int firstFloorToGo = destinationFloors.get(0);
        Direction direction = Direction.getDirection(floorNumber, firstFloorToGo);
        requestDirection = direction;
    }

    @Override
    public boolean sameDirectionMergedRequest() {
        return boardingFloors != null;
    }


    public FloorEvent(Direction direction, List<Integer> boardingFloors, List<Integer> compactedDest) {

        this.floorNumber = null;
        //you either process"Regular" FloorRequest
        //or you have a FloorRequestGroup, where you combined multiple requests
        this.boardingFloors = boardingFloors;

        //Nu mai e nevoie de Sortare, ca deja au fost sortate
        //FALS. mai trebuiesc sortate ca adaugi din mai multe liste, si ordinea se pierde
        FloorEvent.sortTargetDestinations(compactedDest, direction);
        destinationFloors.addAll(compactedDest);

        this.requestDirection = direction;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    /**
     * The first Target Floor where we have to go/drop off people is
     * UP,ABOVE or DOWN,below the floor where we picked-up
     */
    @Override
    public Direction getRequestDirection() {
        return requestDirection;
    }

    @Override
    public Stream<Integer> getBoardingFloors() {
        return floorNumber != null ? Stream.of(floorNumber) : boardingFloors.stream();
    }

    /**
     * @return the floors where we have to take the passengers,
     * </p>
     * Sorted/Optimized so that for this FloorRequest the elevator will take the shortest Route
     */
//    public List<Integer> getDestinationFloors() {
//        return destinationFloors;
//    }
    public Stream<Integer> getDestinationFloors() {
        return destinationFloors.stream();
    }

    @Override
    public String toString() {
        return requestDirection + " | from[" + floorNumber + "] to: " + destinationFloors;
    }

    private void sortTargetDestinations(List<Integer> targetDestinations) {
        TargetFloorsDirection direction = TargetFloorsDirection.getDirection(floorNumber, targetDestinations);
        switch (direction) {
            case UP:
                //all the target floors are ABOVE/UP the current floor so we visit them in ASC order
                targetDestinations.sort(Integer::compareTo);
                break;
            case DOWN:
                //all the target floors are BELOW/DOWN the current floor so visit them in DESC order
                targetDestinations.sort(Comparator.comparingInt(Integer::intValue).reversed());
                break;
            default:
                //it is highly unlikely that the Lift Car would go opposite directions,
                //but in that case, don't sort the floors, leave them as is
                break;
        }
    }

    public static void sortTargetDestinations(List<Integer> targetDestinations, Direction direction) {

        switch (direction) {
            case UP:
                //all the target floors are ABOVE/UP the current floor so we visit them in ASC order
                targetDestinations.sort(Integer::compareTo);
                break;
            case DOWN:
                //all the target floors are BELOW/DOWN the current floor so visit them in DESC order
                targetDestinations.sort(Comparator.comparingInt(Integer::intValue).reversed());
                break;
            default:
                throw new IllegalStateException("You can reach this point, a direction must be specified!");
        }
    }

}
