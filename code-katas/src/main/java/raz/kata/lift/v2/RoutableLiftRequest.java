package raz.kata.lift.v2;


import raz.kata.lift.Direction;

import java.util.stream.Stream;

/**
 * A LiftRequest which must be processed by the LiftCart
 */
public interface RoutableLiftRequest {

    Stream<Integer> getBoardingFloors();

    Stream<Integer> getDestinationFloors();

    boolean sameDirectionMergedRequest();

    Direction getRequestDirection();

}
