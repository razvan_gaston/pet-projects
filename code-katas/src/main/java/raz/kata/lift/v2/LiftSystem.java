package raz.kata.lift.v2;

import raz.kata.lift.ILiftManager;
import raz.kata.lift.LiftCar;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

public class LiftSystem implements ILiftManager {


    Queue<FloorEvent> floorEventsQueue = new LinkedList<>();

    LiftCar liftCar = new LiftCar();

    @Override
    public List<Integer> processQueue(Map<Integer, List<Integer>> floorCallsQueue) {

        //We don't have a Live Event System which keeps adding events to be processed,
        //so we work with SNAPSHOTS(Istantanee) representing different Scenarios
        //So we don't react/process Live Events
        //BUT
        //Here we Load the Scenario/SNAPSHOT
        for (Map.Entry<Integer, List<Integer>> pickupFloor_DropOffFloors : floorCallsQueue.entrySet()) {
            //EnQueue FloorEvents
            floorEventsQueue.add(new FloorEvent(pickupFloor_DropOffFloors.getKey(), pickupFloor_DropOffFloors.getValue()));
        }

        //Once that Scenario/SNAPSHOT is loaded, we process/consume the queue
        processQ();

        return liftCar.itinerary();
    }

    private void processQ() {
        if (this.floorEventsQueue.isEmpty())
            return;

        consumeFloorEvent(floorEventsQueue.poll());
//        if(!floorEventsQueue.isEmpty()){
//            consumeFloorEvent(floorEventsQueue.poll());
//        }
    }

    private void consumeFloorEvent(FloorEvent floorEvent) {

        //move the Lift Car to the FloorEvent
        liftCar.pickupFrom(floorEvent.getFloorNumber());

        //if buttons we pressed, we need to take the ppl to the desired Destination Floors
        if(floorEvent.getDestinationFloors().count()!=0){
            liftCar.dropOffTo(floorEvent.getDestinationFloors().collect(Collectors.toList()));
        }


        if (!floorEventsQueue.isEmpty()) {
            consumeFloorEvent(floorEventsQueue.poll());
        }
    }
}
