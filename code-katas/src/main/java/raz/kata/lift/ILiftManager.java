package raz.kata.lift;

import java.util.List;
import java.util.Map;

public interface ILiftManager {
    List<Integer> processQueue(Map<Integer, List<Integer>> floorCallsQueue);

    default int getFloorsMovesCount(){
        return 0;
    }
}
