package raz.kata.lift;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LiftCar {
    /*
- The Lift starts from the Ground Floor,
- If the lift is empty, and no people are waiting, then it will return to the ground floor*/
    static final int restPosition = 0;
    private final List<Integer> itinerary = new ArrayList<>();

    private int currentFloor;

    public LiftCar() {
        this.currentFloor = restPosition;
        itinerary.add(currentFloor);
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void pickupFrom(int requestFromFloor) {
        if (currentFloor != requestFromFloor) {
            goToFloor(requestFromFloor);
        }
    }

    public void dropOffTo(List<Integer> destinationsFloors) {

        //TODO remove this, as sorting is already done in FloorEvent
        //The order of the Floors to visit, depends on the Direction Going UP/DOWN?
        //of the Target Floors relative to the Current Floor
//        TargetFloorsDirection direction = TargetFloorsDirection.getDirection(currentFloor, destinationsFloors);
//        switch (direction) {
//            case UP:
//                //all the target floors are ABOVE/UP the current floor so we visit them in ASC order
//                destinationsFloors.sort(Integer::compareTo);
//                break;
//            case DOWN:
//                //all the target floors are BELOW/DOWN the current floor so visit them in DESC order
//                destinationsFloors.sort(Comparator.comparingInt(Integer::intValue).reversed());
//                break;
//            default:
//                break;
//        }


        //from the currentFloor/pickUp Floor, I must drop off the persons to their
        //destination floors, But in what order?
        // in the order they were requested?


        destinationsFloors.stream()
                .forEach(this::goToFloor);
    }

    public List<Integer> itinerary() {
        return itinerary;
    }

    private void goToFloor(int nextFloor) {
        itinerary.add(nextFloor);
        this.currentFloor = nextFloor;
    }
}
