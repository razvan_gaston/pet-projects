package raz.kata.util;

import raz.kata.game_of_life.RowColPair;

import java.util.function.Function;

public class MatrixUtil {

    public static <T> void showMatrix(T[][] matrix) {
        if (matrix == null) {
            System.out.println("Matrix is null");
            return;
        }

        int mainArrElementsNum = matrix.length;

        for (int i = 0; i < mainArrElementsNum; i++) {
            T[] subArray = matrix[i];
            for (int j = 0; j < subArray.length; j++) {
                System.out.print(matrix[i][j]);
                if (j != subArray.length - 1)
                    System.out.print(" | ");
            }
            System.out.println();
        }
    }

    //daca apelezi functia asta, tu trebuie sa ai - T val, obiectul instantiat
    //tu stii valuarea cu care vrei sa intializezi celula, e.g. DEAD, LIVE, etc...
    //DAR, NU STII coordonatele i,j
    public static <T> void initMatrixWithValue(T[][] matrix, T val) {
        if (matrix == null) {
            throw new IllegalArgumentException("The matrix can not be null");
        }

        for (int i = 0; i < matrix.length; i++) {
            T[] oneLine = matrix[i];
            for (int j = 0; j < oneLine.length; j++) {
                oneLine[j] = val;
            }
        }
    }

    /**
     * valProducer - abilitatea sa '~~transmit~~/construiesc direct' in lambda/on the fly parametri dinamici, adica coordonatele celulelor sunt ~~trimise~~ construite
     * dinamic la momentul apelarii lambda-ului
     */
    //or pe varianta asta,
    //tu nu trimiti o instanta de Obiect gata creata, ci doar trimiti un Mapper/Supplier/functie care va fi folosit/executat(xASZ1)
    public static <T> void initMatrixWithValueSupplier(T[][] matrix, /*Supplier<T> valSup, UnaryOperator<T> cellMapper*/ Function<RowColPair, T> mapperFunc) {
        if (matrix == null) {
            throw new IllegalArgumentException("The matrix can not be null");
        }

        for (int rowIdx = 0; rowIdx < matrix.length; rowIdx++) {
            T[] oneLine = matrix[rowIdx];
            for (int colIdx = 0; colIdx < oneLine.length; colIdx++) {
                T result = mapperFunc.apply(new RowColPair(rowIdx, colIdx));//(xASZ1) - only here a Cell object is instantiated
                 /*
                 So by using a Functional Interface, allows you to pass code as parameter down the line
                 which will be used/invoked latter/down the line. Caz concret:
                 aici folosec/execut mapperFunc(rowColPair) care combinat cu codul pe care
                 mi l-ai specificat tu, e.g:
                 (rowColPair) -> new Cell(CellState.DEAD, rowColPair.rowIdx(), rowColPair.colIdx(), gameMatrix, this)
                 intoarce un T object Instance (adica un Cell)
                 */
                oneLine[colIdx] = result;
            }
        }
    }

}
