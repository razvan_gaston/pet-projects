package raz.datastruct.list;

public class Element<T> {

    private final T value;
    private final Element previous;


    /**
     * (package protected)
     * <p>
     * This constructor is ONLY used when creating the head
     */
    Element(T value) {
        this.value = value;

        //For Head, previous is not applicable
        this.previous = null;
    }

    Element(T value, Element previous) {
        this.value = value;
        this.previous = previous;
    }

    Element previous() {
        return previous;
    }

    T value() {
        return value;
    }

    /**
     * it is like hasNext() in iterator
     * hasPrevious()?
     */
    boolean hasLink() {
        return previous != null;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("{");
        sb.append("value=").append(value);
        sb.append(", previous=").append(previous);
        sb.append('}');
        return sb.toString();
    }
}
