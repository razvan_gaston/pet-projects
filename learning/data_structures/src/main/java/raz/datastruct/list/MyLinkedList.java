package raz.datastruct.list;

import java.util.Iterator;
import java.util.Objects;

/**
 * <p>
 * Razvan's Linked List Implementation
 * </p>
 * <p>
 * A List is an ORDERED Collection of Elements
 * </p>
 * What should a List do?
 * - add Elements
 * - keep the order of elements
 * - get elements, in the same order in which they were added
 */
public class MyLinkedList<T>  implements Iterable<T>{

    private Element<T> head = null;
    private Element<T> last;

    public boolean add(T value) {
        if (head == null) {
            head = new Element<>(value);
            last = head;
        } else {
            Element<T> elem = new Element(value, last);
            last = elem;
        }

        return true;
    }

    public boolean remove(T value) {
        head = null;
        return true;
    }

    boolean contains(Object o) {
        if (head == null) {
            //this list is empty
            return false;
        } else {
            boolean contains;
            //Iterate
            Element current = last;
            do {
                Object val = current.value();
                contains = Objects.equals(val, o);

                //go to previous elem/Advance/move thru the list
                current = current.hasLink() ? current.previous() : null;
            } while (current != null && !contains);
            return contains;
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MyLinkedList{");
        addToSB(sb, last);
        sb.append('}');
        return sb.toString();
    }


    private void addToSB(StringBuilder sb, Element element) {
        if (!element.hasLink()) {
            sb.append(element.value());
        } else {
            addToSB(sb, element.previous());
            sb.append(",");
            sb.append(element.value());
        }
    }

    /**
     * Advance/move thru the list to the desired element
     * <p>
     * The client should check if the list is empty
     */
    public String printList() {
        StringBuilder sb = new StringBuilder("{");
        if(head==null){
            //this list is empty
            sb.append("}");
            return sb.toString();
        }

        //LOOP - recursion
        Element oneElem = traverseToHead(last);
        sb.append(oneElem.value());
        sb.append(",");
        //END LOOP

        sb.append("}");

        return sb.toString();
    }



    private Element traverseToHead(Element element){
        if(!element.hasLink()){
            return element;
        } else {
            return traverseToHead(element.previous());
        }
    }

    @Override
    public Iterator<T> iterator() {
        return  new MyIterator(this, this.last);
    }

    //    public int size() {
//        return 0;
//    }
//
//    public boolean isEmpty() {
//        return false;
//    }
//
//    public Object get(int index) {
//        return null;
//    }
//
//    public Object remove(int index) {
//        return null;
//    }

    public class MyIterator<T>  implements Iterator<T>{

        final MyLinkedList<T> linkedList;
        final Element<T> listLastElem;

        private Element<T> currentPosition = null;


        private MyIterator(MyLinkedList<T> linkedList,
                           Element<T> listLastElem) {
            this.linkedList = linkedList;
            this.listLastElem = listLastElem;
        }

        public T next(){
            if(currentPosition == null){
                currentPosition = listLastElem;
            } else {
                if(currentPosition.previous()!=null){
                    //IF we're not on the head, advance
                    currentPosition = currentPosition.previous();
                }
            }
            return  currentPosition.value();
        }

        public boolean hasNext(){
            return currentPosition==null || currentPosition.hasLink() ;
        }
    }
}
