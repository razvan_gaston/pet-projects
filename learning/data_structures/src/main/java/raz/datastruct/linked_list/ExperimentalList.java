package raz.datastruct.linked_list;

import raz.datastruct.list.Element;

public class ExperimentalList<T> extends LinkedList<T> {

    public ExperimentalList() {
        super();
    }


    /**
     * XXX: Poate ca asa functioneaza constructuorul din LikedList?!
     * - adica new List(List) - nu inseamna ca sa creeaza o noua colectie din colectia existenta
     * - ci doar ca, noua colectie are acelasi head, like the head of the given collection
     */
    public ExperimentalList(ListNode<T> head) {
        this.head = head;
        this.last = head;
    }



}
