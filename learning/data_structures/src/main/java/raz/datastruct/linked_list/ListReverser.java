package raz.datastruct.linked_list;

public class ListReverser<T> {

    /**
     * @return the Head Node of the reversed collection
     */
    public ListNode<T> reverse(ListNode<T> listHead) {
        if (listHead == null)
            return null;
        else
            return reverseNode(listHead);
    }

    private ListNode<T> reverseNode(ListNode origCursor) {
        return reverseNode(origCursor, null);
    }

    private ListNode<T> reverseNode(ListNode origCursor, ListNode previousProcessed) {
        ListNode<T> currentlyProcessed;
        currentlyProcessed = processNode(origCursor, previousProcessed);//prima data il proceseaza pe primul/current,
        if (origCursor.hasNex()) {
            // apoi se re-apeleaza pt urmatorul node, folosind ce s-a calculat pt nodul curent
            currentlyProcessed = reverseNode(origCursor.next(), currentlyProcessed);
        }
        return currentlyProcessed;
    }

    private ListNode<T> processNode(ListNode origNode, ListNode previousProcessed) {
        ListNode<T> nodeCopy = new ListNode<>(origNode);
        nodeCopy.setNext(previousProcessed);
        return nodeCopy;
    }
}
