package raz.datastruct.linked_list;

import java.util.Iterator;
import java.util.NoSuchElementException;

/*
I've implemented this as an inner class, in order to have access to private
field like last and head.
* */
class MyIteratorImpl<T> implements Iterator<T> {

    //the iterator backing collection is:
    private final LinkedList<T> theList;
    private final ListNode<T> listHead;

    private ListNode<T> cursor;

    MyIteratorImpl(LinkedList<T> list, ListNode head) {
        theList = list;
        this.listHead = head;
    }

    @Override
    public boolean hasNext() {
        if (theList.isVoid())
            return false;

        if (cursor == null) {
            //it is the first time use use this iterator,
            // and next() was not called yet, we have at least the head
            return true;
        } else {
            return cursor.hasNex();
        }
    }

    @Override
    public T next() {
        nextNode();
        if (cursor == null) {
            //the client Broke the Contract of Iterator,
            //he has called next() without checking first with hasNext()
            throw new NoSuchElementException();
        } else {
            return cursor.value();
        }
    }

    /* Advance the Cursor thru the List*/
    private void nextNode() {
        if (cursor == null) {
            //it is the first time we use this iterator
            cursor = listHead;
        } else {
            cursor = cursor.next();
        }
    }
}
