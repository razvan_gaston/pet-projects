package raz.datastruct.linked_list;

import java.util.Iterator;
import java.util.Objects;

public class LinkedList<T> implements Iterable<T> {

    protected ListNode<T> head = null;
    protected ListNode<T> last = null;

    /**
     * Default Constructor
     */
    public LinkedList() {
    }

    public void add(T value) {
        if (isVoid()) {
            head = ListNode.headNode(value);
            last = head;
        } else {
            last = ListNode.addNode(value, last);
        }
    }

    public boolean contains(T value) {
        if (isVoid())
            return false;
        else {
            boolean contains;
            ListNode<T> current = head;
            do {
                T val = current.value();
                contains = Objects.equals(val, value);

                //go to next element, Advance/move thru the list
                current = current.next() != null ? current.next() : null;
            } while (current != null && !contains);

            return contains;
        }
    }

    public void remove(T a) {
        head = null;
    }

    /**
     * If we don't have any head... this list is empty/void
     */
    boolean isVoid() {
        return head == null;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MyList{");
        if (!isVoid()) {
            addToSB(sb, head);
        }
        sb.append('}');
        return sb.toString();
    }

    private void addToSB(StringBuilder sb, ListNode<T> first) {
        sb.append(first.value());
        if (first.next() != null) {
            sb.append(",");
            addToSB(sb, first.next());
        }
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        LinkedList<?> that = (LinkedList<?>) other;
        //nu poti sa implenentezi equals pina nu ai get the index?
        //sau se incerc cu iterator:
        if (this.isVoid()) {
            return that.isVoid();
        } else {
            Iterator thisIter = this.iterator();
            Iterator thatIter = that.iterator();
            boolean areElementsEqual = true;
            while (thisIter.hasNext() && areElementsEqual) {
                //Compare elements side by side
                areElementsEqual = thisIter.next().equals(thatIter.next());
            }
            return areElementsEqual;
        }
    }


    @Override
    public Iterator<T> iterator() {
        return new MyIteratorImpl(this, head);
    }
}
