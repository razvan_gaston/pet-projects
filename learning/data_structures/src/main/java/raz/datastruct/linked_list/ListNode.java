package raz.datastruct.linked_list;

import java.util.Objects;

public class ListNode<T> {

    private final T value;
    private ListNode next;

    ListNode(T value) {
        this.value = value;
    }

    /**
     * Copy Constructor
     */
    ListNode(ListNode<T> other) {
        this(other.value);
        this.next = other.next;
    }

    public static <T> ListNode<T> headNode(T value) {
        return new ListNode<>(value);
    }

    //factory method instead of constructor?
    public static <T> ListNode<T> addNode(T value, ListNode previous) {

        ListNode<T> newNode = new ListNode<>(value);
        previous.next = newNode;
        return newNode;
    }

    public ListNode next() {
        return next;
    }

    public void setNext(ListNode node) {
        this.next = node;
    }

    public T value() {
        return value;
    }

    @Override
    public String toString() {
        return "[" +
                "value=" + value +
                ", next=" + (next == null ? null : next.value) +
                ']';
    }

    public boolean hasNex() {
        return next != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListNode<?> listNode = (ListNode<?>) o;
        return Objects.equals(value, listNode.value) && Objects.equals(next, listNode.next);
    }
}
