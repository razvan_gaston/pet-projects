package raz.datastruct.tree;

import java.util.ArrayList;
import java.util.List;

public class TNode<T> {

    private final T value;
    private final TNode parent;
    private final List<TNode<T>> children = new ArrayList<>();

    /**
     * Use this ONLY for ROOT of a tree
     */
    public TNode(T value) {
        this.value = value;
        this.parent = null;
    }

    public TNode(T value, TNode parent) {
        this.value = value;
        this.parent = parent;
    }

    /**
     * @return the newly added node
     */
    public TNode addChild(T value) {
        TNode child = new TNode<T>(value, this);
        children.add(child);
        return child;
    }

    public T getValue() {
        return value;
    }

    public List<TNode<T>> getChildren() {
        return children;
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    @Override
    public String toString() {
        return "{" +
                "" + value +
                '}';
    }
}
