package raz.datastruct.tree;


public class Tree<T> {

    final TNode root;

    public Tree(TNode root) {
        this.root = root;
    }

    public Tree(String val){
        root = new TNode(val);
    }

    public TNode getRoot() {
        return root;
    }

    /**
     * convenience method adds a new value(TreeNode) to this Tree
     */
    public TNode add(T value) {
        return root.addChild(value);
    }

    /**
     * Get the Element from the Tree
     * - it iterates the tree and finds the node with the given value
     * - if not found returns null
     * */
    //public TNode find(T value){}
}
