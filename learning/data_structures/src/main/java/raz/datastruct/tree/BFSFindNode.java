package raz.datastruct.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Breadth First Search to find a certain Node/Value in the Tree
 * recursion + for loop
 * <p>
 * --
 * L.E
 * - asta merge, dar nu e Breadth First Search, cred ca este
 * Depth First Search, pt ca, pe o anumita ierarhie se duce pina cand nu mai are copii, and only after it exhausts it
 * comes back to other children
 */
public class BFSFindNode<T> {

    final Tree<T> tree;

    public int nodesVisits = 0;

    T theValue;

    public BFSFindNode(Tree<T> tree) {
        this.tree = tree;
    }

    public List<T> pathTo(T value) {
        List<T> pathToValue = new ArrayList<>();
        TNode root = tree.getRoot();
        theValue = value;
        recursive(root, new CanContinue(), pathToValue);
//        CanContinue nodeVisit = visitNode(root);
//        if(!nodeVisit.doContinue()){
//            if(nodeVisit.isPresent()){
//                T val = (T) nodeVisit.getSearchedElement().getValue();
//                pathToValue.add(val);
//            }
//        } else {
//            aa(r)
//        }
        Collections.reverse(pathToValue);
        return pathToValue;
    }

    <T> void recursive(TNode<T> parent, CanContinue nodeVisit, List<T> pathToValue) {
        visitNode(parent, nodeVisit);
        System.out.println(parent);
        if (!nodeVisit.doContinue()) {
            if (nodeVisit.isPresent()) {
                T val = (T) nodeVisit.getSearchedElement().getValue();
                pathToValue.add(val);
            }
        } else {
            for (TNode<T> oneChild : parent.getChildren()) {

                recursive(oneChild, nodeVisit, pathToValue);
                if (nodeVisit.isPresent()) {
                    pathToValue.add(parent.getValue());
                    //if we already found the value, do not continue to other children/hierarchies
                    break;
                }
                //return;
            }
        }
    }

    void visitNode(TNode parent, CanContinue nodeVisit) {
        nodesVisits++;
        //CanContinue canContinue =  new CanContinue();
        if (!parent.hasChildren() || parent.getValue().equals(theValue)) {
            nodeVisit.setCanContinue(false);
            if (parent.getValue().equals(theValue)) {
                nodeVisit.setSearchedElement(parent);
            }
        } else {
            nodeVisit.setCanContinue(true);
        }
//        return void;
    }
}
