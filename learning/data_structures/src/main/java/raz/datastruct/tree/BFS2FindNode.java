package raz.datastruct.tree;

import java.util.*;
import java.util.stream.Collectors;

public class BFS2FindNode<T> {

    final Tree<T> tree;
    T theValue;
    TNode<T> found;

    Queue<TNode<T>> nodesToVisit = new LinkedList<>();
    Set<TNode<T>> alreadyVisited = new HashSet<>();
    Map<TNode, TNode> childToParent = new HashMap<>();

    public int nodesVisits = 0;

    public BFS2FindNode(Tree<T> tree) {
        this.tree = tree;
    }

    public List<T> pathTo(T value) {
        theValue = value;
        List<TNode<T>> pathToValue = nodesToExit();
        if (pathToValue != null)
            return pathToValue
                    .stream()
                    .map(e -> e.getValue())
                    .collect(Collectors.toList());
        else
            return null;
    }

    public List<TNode<T>> nodesToExit() {
        List<TNode<T>> pathToValue = null;
        nodesToVisit.add(tree.root);

        boolean valueFound = false;
        while (!nodesToVisit.isEmpty() && !valueFound) {
            valueFound = visitNode(nodesToVisit.remove());
        }

        pathToValue = reconstructPath();

        return pathToValue;
    }


    private List<TNode<T>> reconstructPath() {
        List<TNode<T>> path = null;
        Stack<TNode<T>> stack = new Stack<>();

        if (found != null) {
            path = new ArrayList<>();
            TNode<T> current = found;
            stack.push(current);
            do {
                current = childToParent.get(current);
                if (current != null) {
                    stack.push(current);
                }
            } while (current != null);

            while (!stack.empty()) {
                path.add(stack.pop());
            }
        }
        return path;
    }

    //visit/CheckNode
    private boolean visitNode(TNode<T> parent) {
        nodesVisits++;
        System.out.println("visiting node:" + parent);
        alreadyVisited.add(parent);
        //childToParent.put()

        List<TNode<T>> children = parent.getChildren()
                .stream()
                .filter(e -> !alreadyVisited.contains(e))
                .collect(Collectors.toList());

        Optional<TNode<T>> value = children.stream()
                .filter(e -> e.getValue().equals(theValue))
                .findFirst();

        if (value.isPresent()) {
            childToParent.put(value.get(), parent);
            found = value.get();
        } else {
            for (TNode<T> oneChild : parent.getChildren()) {
                nodesToVisit.offer(oneChild);
                childToParent.put(oneChild, parent);
            }
        }

        return value.isPresent();
    }

}
