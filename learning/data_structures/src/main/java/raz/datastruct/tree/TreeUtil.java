package raz.datastruct.tree;

import java.util.List;

public class TreeUtil {

    static String LEVEL_INDENTATION = "+--";
    static String LEVEL_INDENTATION_INCREMENT = "   ";

    public <T> void showTree(Tree<T> tree) {
        StringBuilder sb = new StringBuilder();
        TNode root = tree.getRoot();
        sb.append(root.getValue());
        showLevel(root.getChildren(), sb, LEVEL_INDENTATION);

        System.out.println(sb);
    }

    public void printTree(Tree tree) {
        StringBuilder sb = new StringBuilder();
        TNode root = tree.getRoot();

        showNode(root, sb, LEVEL_INDENTATION, 1);

        System.out.println(sb.toString());
    }

    void showNode(TNode parent, StringBuilder prev, String levelIndentation, int level) {
        prev.append(parent.getValue());
        if (parent.hasChildren()) {
            String levelIndent = level == 1 ? LEVEL_INDENTATION : LEVEL_INDENTATION_INCREMENT + levelIndentation;
            //add current value
            List<TNode> children = parent.getChildren();
            for (TNode child : children) {
                //Prepare Indentation for the next level which will be added
                prev.append("\n");
                prev.append(levelIndent);
                showNode(child, prev, levelIndent, level+1);
            }
        }
    }

    void showLevel(List<TNode> children, StringBuilder parenLevel, String levelIndentation) {
        //StringBuilder sb = new StringBuilder();
        if (children.isEmpty()) {
            //parenLevel.append(sb);
            return;
        } else {
            for (TNode child : children) {
                parenLevel.append("\n");
                parenLevel.append(levelIndentation);
                parenLevel.append(child.getValue());

                showLevel(child.getChildren(), parenLevel, LEVEL_INDENTATION_INCREMENT + LEVEL_INDENTATION);
            }
        }
    }
}
