package raz.datastruct.tree;

import java.util.Optional;

public class CanContinue<T> {

    boolean canContinue;
    Optional<TNode<T>> searchedElement = Optional.empty();

    public boolean doContinue() {
        return canContinue;
    }

    public void setCanContinue(boolean canContinue) {
        this.canContinue = canContinue;
    }

    public TNode<T> getSearchedElement() {
        return searchedElement.get();
    }

    public void  setSearchedElement(TNode<T> searchedElement) {
        this.searchedElement = Optional.of(searchedElement);
    }

    public boolean isPresent() {
        return searchedElement.isPresent();
    }

    @Override
    public String toString() {
        return "CanContinue{" +
                "canContinue=" + canContinue +
                ", searchedElement=" + searchedElement +
                '}';
    }
}
