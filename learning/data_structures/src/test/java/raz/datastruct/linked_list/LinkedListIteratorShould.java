package raz.datastruct.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import raz.datastruct.list.MyLinkedList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListIteratorShould {

    LinkedList<String> alphabet = null;

    @BeforeEach
    void setUp() {
        alphabet = new LinkedList();
    }

    @Test
    void hasNext_answers_true_only_one_element() {
        //GIVEN
        alphabet.add("a");
        Iterator<String> it = alphabet.iterator();

        //WHEN
        boolean hasNext = it.hasNext();

        //THEN
        assertTrue(hasNext);

    }

    @Test
    void hasNext_answers_false_void_list() {
        //GIVEN
        LinkedList<String> alphabet = new LinkedList();
        Iterator<String> it = alphabet.iterator();

        //WHEN
        boolean hasNext = it.hasNext();

        //THEN
        assertFalse(hasNext);
    }

    @Test
    void iterate_only_one_element() {
        //GIVEN
        alphabet.add("a");
        Iterator<String> it = alphabet.iterator();

        //WHEN
        String elem = null;
        while (it.hasNext()) {
            elem = it.next();
        }


        //THEN
        assertEquals("a", elem);
    }

    @Test()
    void throw_NoSuchElementException_if_I_call_next_without_1st_checking_if_we_have_next() {
        //GIVEN
        LinkedList<String> alphabet = new LinkedList();
        alphabet.add("a");
        Iterator<String> it = alphabet.iterator();

        //Get the 1st ELEM
        String elem = null;
        elem = it.next();
        assertEquals("a", elem);


        //Call next again, without checking with hasNext()
        //WHEN
        Assertions.assertThrows(NoSuchElementException.class, () -> {
            it.next();
        });

        //THEN
    }

    @Test
    void iterate_more_elements() {
        //GIVEN
        alphabet.add("a");
        alphabet.add("b");
        alphabet.add("c");
        Iterator<String> it = alphabet.iterator();

        //WHEN
        String elem = null;

        elem = it.next();
        System.out.println(elem);
        assertEquals("a", elem);

        elem = it.next();
        System.out.println(elem);
        assertEquals("b", elem);

        elem = it.next();
        System.out.println(elem);
        assertEquals("c", elem);

    }

    @Test
    void iterate_elements_in_for_each_construct() {
        //GIVEN
        alphabet.add("a");
        alphabet.add("b");
        alphabet.add("c");

        //WHEN
        for(String elem : alphabet){
            System.out.println(elem);
        }
    }

}
