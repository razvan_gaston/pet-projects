package raz.datastruct.linked_list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class IterateRecursive {

    ExperimentalList<String> list;

    @BeforeEach
    void setUp() {
        ListNode<String> head = new ListNode<>("a");
        list = new ExperimentalList<>(head);

        list.add("b");
        list.add("c");
        list.add("d");

    }

    void iterate(ListNode root, List<ListNode> collection){
        if(!root.hasNex()){
            collection.add(root);
        } else {
            //for(ListNode child : root.next()){}
            iterate(root.next(), collection);
            collection.add(root);
        }
    }
    @Test
    void iterate_recursively() {
        List<ListNode> collection = new ArrayList<>();
        iterate(list.head, collection);
        for (ListNode s : collection) {
            System.out.println(s.value());
        }
    }


}
