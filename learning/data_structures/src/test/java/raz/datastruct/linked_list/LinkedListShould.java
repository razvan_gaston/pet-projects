package raz.datastruct.linked_list;

import org.junit.jupiter.api.Test;
import raz.datastruct.list.MyLinkedList;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListShould {

    /**
     * 1/
     * What is the simplest thing a List can do?
     * <p>
     * Add 1 element!
     * It shouldn't be empty
     * Then: (see step 2/)
     */
    @Test
    void retain_only_one_element() {
        //GIVEN
        LinkedList<String> alphabet = new LinkedList();

        //WHEN
        alphabet.add("a");

        //THEN
        assertTrue(alphabet.contains("a"));

        System.out.println(alphabet.toString());
    }

    /**
     * 2/
     * If I remove the Element, then the list is empty
     */
    @Test
    void remove_one_element_from_1_element_list() {
        //GIVEN
        LinkedList<String> alphabet = new LinkedList();

        //WHEN
        alphabet.remove("a");

        //THEN
        assertFalse(alphabet.contains("a"));
    }

    /**
     * 3/
     * Add 2 elements to the list
     */
    @Test
    void retain_2_elements() {
        //GIVEN
        LinkedList<String> alphabet = new LinkedList();

        //WHEN
        alphabet.add("a");
        alphabet.add("b");

        //THEN
        assertTrue(alphabet.contains("a"));
        assertTrue(alphabet.contains("b"));

        System.out.println(alphabet.toString());
    }

    @Test
    void retain_3_elements() {
        //GIVEN
        LinkedList<Integer> alphabet = new LinkedList();

        //WHEN
        alphabet.add(1);
        alphabet.add(2);
        alphabet.add(3);

        //THEN
        assertTrue(alphabet.contains(1));
        assertTrue(alphabet.contains(2));
        assertTrue(alphabet.contains(3));

        System.out.println(alphabet.toString());
    }


    @Test
    void test_toString_0_elements() {
        //GIVEN
        LinkedList<String> alphabet = new LinkedList();

        System.out.println(alphabet.toString());
    }

    @Test
    void test_Equals_2_elements_same_elements_same_order() {
        LinkedList<String> alphabet = new LinkedList();
        alphabet.add("a");
        alphabet.add("b");

        LinkedList<String> alphabet2 = new LinkedList();
        alphabet2.add("a");
        alphabet2.add("b");

        assertTrue(alphabet.equals(alphabet2));
    }

    @Test
    void test_Equals_2_elements_same_elements_different_order() {
        LinkedList<String> alphabet = new LinkedList();
        alphabet.add("a");
        alphabet.add("b");

        LinkedList<String> alphabet2 = new LinkedList();
        alphabet2.add("b");
        alphabet2.add("a");

        assertFalse(alphabet.equals(alphabet2));
    }

    /* I just wanted to use my LinkedList via a Stream*/
    @Test
    void use_the_list_in_a_stream() {
        //GIVEN
        LinkedList<Integer> alphabet = new LinkedList();
        alphabet.add(1);
        alphabet.add(2);
        alphabet.add(3);

        //WHEN
        Stream<Integer> myStream = StreamSupport.stream(alphabet.spliterator(), false);

        System.out.println(myStream.count());

        myStream.peek(System.out::println)
                .allMatch(num -> num < 2);
    }

    //In order to reverse, first I need an Iterator
    //to access the List Elements, because I wan't to reverse it be creating a new List
    @Test
    void reverseCollection() {

    }
}
