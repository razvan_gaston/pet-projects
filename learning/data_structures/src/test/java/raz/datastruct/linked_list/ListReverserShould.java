package raz.datastruct.linked_list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ListReverserShould {
    ListReverser reverser;

    @BeforeEach
    void setUp() {
        reverser = new ListReverser();
    }

    @Test
    void reverse_empty_list() {
        //GIVEN
        ExperimentalList<String> list = new ExperimentalList<>();

        //WHEN
        ListNode<String> reversed = reverser.reverse(list.head);

        //THEN
        assertNull(reversed);
    }

    /**
     * What is the simplest use-case?
     * - for a one node list,
     * - the reversed is the same
     */
    @Test
    void reverse_one_node_list() {
        //GIVEN
        ListNode<String> head = new ListNode<>("a");

        //WHEN
        ListNode<String> reversed = reverser.reverse(head);

        //THEN
        assertEquals(head, reversed);
    }

    @Test
    void reverse_2_nodes_list() {
        //GIVEN
        ListNode<String> head = new ListNode<>("a");
        ExperimentalList<String> list = new ExperimentalList<>(head);
        list.add("b");
        System.out.println("original:" + list);

        ListReverser reverser = new ListReverser();

        //WHEN
        ListNode<String> reversed = reverser.reverse(head);
        ExperimentalList<String> listReversed = new ExperimentalList<>(reversed);

        //THEN
        ExperimentalList<String> expected = new ExperimentalList<>();
        expected.add("b");
        expected.add("a");
        assertEquals(expected, listReversed);

    }

    @Test
    void reverse_3_nodes_list() {
        //GIVEN
        ListNode<String> head = new ListNode<>("a");
        ExperimentalList<String> list = new ExperimentalList<>(head);
        list.add("b");
        list.add("c");
        System.out.println("original:" + list);

        ListReverser reverser = new ListReverser();

        //WHEN
        ListNode<String> reversed = reverser.reverse(head);
        ExperimentalList<String> listReversed = new ExperimentalList<>(reversed);
        for (String s : listReversed) {
            System.out.println(">>" + s);
        }

        //THEN
        ExperimentalList<String> expected = new ExperimentalList<>();
        expected.add("c");
        expected.add("b");
        expected.add("a");
        assertEquals(expected, listReversed);
        System.out.println("reversed:" + listReversed);

    }
}
