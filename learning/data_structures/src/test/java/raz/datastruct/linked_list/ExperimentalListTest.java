package raz.datastruct.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExperimentalListTest {


    @Test
    void iterate_2_elements() {
        ExperimentalList<String> alphabet = new ExperimentalList<>();

        alphabet.add("a");
        alphabet.add("b");

        for(String e : alphabet){
            System.out.println(e);
        }
    }

    /**
     * */
    @Test
    void use_the_list_by_providing_the_head() {

        ListNode<String> head = new ListNode<>("a");
        ExperimentalList<String> list = new ExperimentalList<>(head);

        list.add("b");
        list.add("c");
        list.add("d");

        for(String s : list){
            System.out.println(s);
        }



        ListNode<String> cursorOriginal = head;
        ListNode<String> newHead = null;

        ListNode<String> previousProcessed = null;
        do{
            //copy Node
            ListNode<String> currentlyProcessed = new ListNode<>(cursorOriginal);
            if(cursorOriginal.equals(head)){
                currentlyProcessed.setNext(null);//capul va fi ultimul node
            }else {
                currentlyProcessed.setNext(previousProcessed);
            }

            //Before I move thru the list, cache the last processed
            previousProcessed = currentlyProcessed;

            if(!cursorOriginal.hasNex()){
                //am ajuns la ultimul elemnt in lista originala, care va fi capul listei reversed
                newHead = currentlyProcessed;
            }
            //go to next element, Advance/move thru the list
            cursorOriginal = cursorOriginal.next() != null ? cursorOriginal.next() : null;


        }while (cursorOriginal!=null);

        ExperimentalList<String> reversedList = new ExperimentalList<>(newHead);

        System.out.println("Reversed:");
        for(String s: reversedList){
            System.out.println(s);
        }
    }

}
