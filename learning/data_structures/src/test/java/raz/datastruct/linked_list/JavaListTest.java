package raz.datastruct.linked_list;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JavaListTest {

    @Test
    void iterate_only_one_element() {
        //GIVEN
        List<String> alphabet = new ArrayList<>();
        alphabet.add("a");
        Iterator<String> it = alphabet.iterator();

        //WHEN
        String elem = null;
        while (it.hasNext()) {
            elem = it.next();
        }


        //THEN
        assertEquals("a", elem);
    }

    @Test()
    void what_happens_if_I_call_next_without_checking_if_we_have_next() {
        //GIVEN
        List<String> alphabet = new ArrayList<>();
        alphabet.add("a");
        Iterator<String> it = alphabet.iterator();

        String elem = null;
        elem = it.next();
        assertEquals("a", elem);

        //WHEN
        Assertions.assertThrows(NoSuchElementException.class, ()->{it.next();});

        //THEN



    }


}
