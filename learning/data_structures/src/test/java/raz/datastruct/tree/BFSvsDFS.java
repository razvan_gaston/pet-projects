package raz.datastruct.tree;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BFSvsDFS {

    static TreeUtil treeUtil = new TreeUtil();

    Tree<String> alphabet = null;

    @Test
    void simple_use_case_3_nodes() {
        alphabet = new Tree<>("a");

        TNode bNode = alphabet.add("b");
        TNode dNode = bNode.addChild("d");
        /*TNode fNode =*/ dNode.addChild("f").addChild("h");
        bNode.addChild("e");

        TNode cNode = alphabet.add("c");
        cNode.addChild("i");
        TNode jNode = cNode.addChild("j");
        jNode.addChild("k");

        TNode yNode = alphabet.add("y");
        yNode.addChild("z");

        treeUtil.printTree(alphabet);
        BFSFindNode<String> bfsFinder = new BFSFindNode<>(alphabet);

        List<String> result = bfsFinder.pathTo("z");
        System.out.println("DFS >> "+result);
        System.out.println("DFS >> "+bfsFinder.nodesVisits);


        BFS2FindNode<String> theRealBFS = new BFS2FindNode<>(alphabet);
        result = theRealBFS.pathTo("z");
        System.out.println("BFS >>"+result);
        System.out.println("BFS >>"+theRealBFS.nodesVisits);

        //Find "b" in the tree
//        List<String> expected = List.of("a","b");
//        assertEquals(expected, result);
    }

}
