package raz.datastruct.tree;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BFSFindNodeTest {

    static TreeUtil treeUtil = new TreeUtil();

    Tree<String> alphabet = null;

    @Test
    void simple_use_case_3_nodes() {
        alphabet = new Tree<>("a");
        alphabet.add("b");
        alphabet.add("c");

        treeUtil.printTree(alphabet);
        BFSFindNode<String> bfsFinder = new BFSFindNode<>(alphabet);

        List<String> result = bfsFinder.pathTo("b");

        //Find "b" in the tree
        List<String> expected = List.of("a","b");
        assertEquals(expected, result);
    }

    @Test
    void simple_use_case_4_nodes() {
        alphabet = new Tree<>("a");
        TNode bNode = alphabet.add("b");
        TNode dNode = bNode.addChild("d");
        dNode.addChild("e");
        alphabet.add("c");

        treeUtil.printTree(alphabet);
        BFSFindNode<String> bfsFinder = new BFSFindNode<>(alphabet);

        List<String> result = bfsFinder.pathTo("e");

        List<String> expected = List.of("a","b","d","e");
        assertEquals(expected, result);
    }

    @Test
    void should_find_path() {
        alphabet = new Tree<>("a");
        TNode bNode = alphabet.add("b");

        TNode cNode = alphabet.add("c");
        cNode.addChild("i");
        cNode.addChild("j");

        TNode eNode = bNode.addChild("e");
        eNode.addChild("z");
        eNode.addChild("y");

        TNode fNode = bNode.addChild("f");
        fNode.addChild("k");

        bNode.addChild("g");

        treeUtil.printTree(alphabet);
        BFSFindNode<String> bfsFinder = new BFSFindNode<>(alphabet);

        List<String> result = bfsFinder.pathTo("y");

        List<String> expected = List.of("a","b", "e", "y");
        System.out.println(result);
        assertEquals(expected, result);
    }

    @Test
    void should_find__deeper_path() {
        alphabet = new Tree<>("a");
        TNode bNode = alphabet.add("b");

        TNode cNode = alphabet.add("c");
        cNode.addChild("i");
        cNode.addChild("j");

        TNode eNode = bNode.addChild("e");
        TNode zNode = eNode.addChild("z");
        eNode.addChild("y");

        zNode.addChild("o");
        TNode mNode = zNode.addChild("m");
        mNode.addChild("s");

        TNode fNode = bNode.addChild("f");
        fNode.addChild("k");

        bNode.addChild("g");

        treeUtil.printTree(alphabet);
        BFSFindNode<String> bfsFinder = new BFSFindNode<>(alphabet);

        List<String> result = bfsFinder.pathTo("s");

        List<String> expected = List.of("a","b", "e", "z", "m", "s");
        System.out.println(result);
        assertEquals(expected, result);
    }

    @Test
    void should_find__deeper_path_q() {
        alphabet = new Tree<>("a");
        TNode bNode = alphabet.add("b");

        TNode cNode = alphabet.add("c");
        cNode.addChild("i");
        cNode.addChild("j");

        TNode eNode = bNode.addChild("e");
        TNode zNode = eNode.addChild("z");
        eNode.addChild("y");

        zNode.addChild("o");
        TNode mNode = zNode.addChild("m");
        mNode.addChild("s");

        TNode fNode = bNode.addChild("f");
        TNode kNode  = fNode.addChild("k");
        kNode.addChild("q");

        bNode.addChild("g");

        treeUtil.printTree(alphabet);
        BFSFindNode<String> bfsFinder = new BFSFindNode<>(alphabet);

        List<String> result = bfsFinder.pathTo("q");

        List<String> expected = List.of("a","b", "e", "z", "m", "s");
        System.out.println(result);
        //assertEquals(expected, result);
    }


}
