package raz.datastruct.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TreeTest {

    static TreeUtil treeUtil = new TreeUtil();

    @Test
    void alphabet_tree_structure_4_levels() {
        TNode<String> a_root = new TNode<>("a");

        Tree<String> alphabet = new Tree<>(a_root);

        TNode bNode = alphabet.add("b");
        alphabet.add("c");

        //TNode bNode = alphabet.find("b");

        TNode fNode = bNode.addChild("f");
        fNode.addChild("k");

        //MatrixUtil.showTree(alphabet);

        treeUtil.showTree(alphabet);
    }

    @Test
    void alphabet_tree_structure_2_levels() {
        TNode<String> a_root = new TNode<>("a");

        Tree<String> alphabet = new Tree<>(a_root);

        TNode bNode = alphabet.add("b");


        treeUtil.showTree(alphabet);
    }
}
