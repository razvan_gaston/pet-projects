package raz.datastruct.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TreeUtilShould {

    static TreeUtil treeUtil = new TreeUtil();

    Tree<String> alphabet = null;

    @Test
    void print_one_level_just_root() {

        //GIVEN - just an one node Tree
        TNode<String> a_root = new TNode<>("a");
        alphabet = new Tree<>(a_root);


        treeUtil.printTree(alphabet);
    }

    @Test
    void print_root_1_child() {

        //GIVEN - just an one node Tree
        //TNode<String> a_root = new TNode<>("a");
        alphabet = new Tree<>("a");
        alphabet.add("b");

        treeUtil.printTree(alphabet);
    }

    @Test
    void print_root_2_children() {

        //GIVEN - just an one node Tree
        //TNode<String> a_root = new TNode<>("a");
        alphabet = new Tree<>("a");
        alphabet.add("b");
        alphabet.add("c");

        treeUtil.printTree(alphabet);
    }

    @Test
    void print_root_2_children_bNode_1_child() {

        //GIVEN - just an one node Tree
        //TNode<String> a_root = new TNode<>("a");
        alphabet = new Tree<>("a");
        TNode bNode = alphabet.add("b");
        alphabet.add("c");

        bNode.addChild("1");

        treeUtil.printTree(alphabet);
    }

    @Test
    void alphabet_tree_structure_4_levels() {
        TNode<String> a_root = new TNode<>("a");

        Tree<String> alphabet = new Tree<>(a_root);

        TNode bNode = alphabet.add("b");
        alphabet.add("c");

        //TNode bNode = alphabet.find("b");

        TNode fNode = bNode.addChild("f");
        fNode.addChild("k");

        //MatrixUtil.showTree(alphabet);

        treeUtil.printTree(alphabet);
    }
}
