package raz.datastruct.list;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.Iterator;

/**
 * I am developing this TDD, Step by Step, ONE VERY SIMPLE Step at a time!
 */
class MyLinkedListShould {

    /**
     * 1/
     * What is the simplest thing a List can do?
     * <p>
     * Add 1 element!
     * It shouldn't be empty
     * Then: (see step 2/)
     */
    @Test
    void retain_one_element() {
        //GIVEN
        MyLinkedList<String> alphabet = new MyLinkedList();

        //WHEN
        alphabet.add("a");

        //THEN
        assertTrue(alphabet.contains("a"));

        System.out.println(alphabet.toString());
    }

    /**
     * 2/
     * If I remove the Element, then the list is empty
     */
    @Test
    void remove_one_element_from_1_element_list() {
        //GIVEN
        MyLinkedList<String> alphabet = new MyLinkedList();
        alphabet.add("a");

        //WHEN
        alphabet.remove("a");

        //THEN
        assertFalse(alphabet.contains("a"));
    }

    @Test
    void retain_2_elements() {
        //GIVEN
        MyLinkedList<String> alphabet = new MyLinkedList();

        //WHEN
        alphabet.add("a");
        alphabet.add("b");

        //THEN
        assertTrue(alphabet.contains("a"));
        assertTrue(alphabet.contains("b"));
        //assertEquals(2, alphabet.size());

        System.out.println(alphabet.toString());
    }

    @Test
    void retain_3_elements() {
        //GIVEN
        MyLinkedList<String> alphabet = new MyLinkedList();

        //WHEN
        alphabet.add("a");
        alphabet.add("b");
        alphabet.add("c");

        //THEN
        assertTrue(alphabet.contains("a"));
        assertTrue(alphabet.contains("b"));
        assertTrue(alphabet.contains("c"));

        System.out.println(alphabet.toString());

        System.out.println(alphabet.printList());

    }

    @Test
    void iterate_elements() {
        //GIVEN
        MyLinkedList<String> alphabet = new MyLinkedList();
        alphabet.add("a");
        alphabet.add("b");
        alphabet.add("c");

        //WHEN
        Iterator<String> iterator = alphabet.iterator();
        String elem = null;
        while (iterator.hasNext()) {
            elem = iterator.next();

            System.out.println(elem);
        }
    }

    @Test
    void iterate_elements_in_for_each_construct() {
        //GIVEN
        MyLinkedList<String> alphabet = new MyLinkedList();
        alphabet.add("a");
        alphabet.add("b");
        alphabet.add("c");

        //WHEN
        for(String elem : alphabet){
            System.out.println(elem);
        }
    }
}
